require 'rubygems'
require 'rspec'
#require 'watir-webdriver'
require 'watir'

require 'page-object'
require 'page-object/page_factory'
require 'rspec/expectations'
require 'data_magic'
require "selenium-webdriver"

include Selenium

World(PageObject::PageFactory)


Before do
  #Selenium::WebDriver::Firefox.driver_path = "C:/Temp/FireFoxDriver/geckodriver-v0.19.1-win32/geckodriver.exe"
  #browser = Watir::Browser.new :firefox

  Selenium::WebDriver::Chrome.driver_path = "C:/Temp/ChromeDriver/chromedriver_win32/chromedriver.exe"
  browser = Watir::Browser.new :chrome

  @browser = browser
end

After do |scenario|
  if scenario.failed?
    @filename = scenario.name
    #@filename = ((((Time.now).to_s).delete(' ')).delete(':')).delete('+')
    @browser.screenshot.save ("C:/Temp/Ruby/DeveloperPortal/features/support/SnapShots/"+@filename+".png")
    embed(@filename, "image/png")
  end
  @browser.close
end

