@DevPortal
Feature: To test Bank Card feature
	I want to use this template for my feature file
	
#Background:
#	Given Developer Portal is opened

@BankCard_get_ping
Scenario: API health get_ping dfghfgh
Given Developer Portal is opened
And I select BankCard
And I select get_ping
When I click on Send this request button
Then response code should be 200
And response body should contain "PONG"

@BankCard_get_status
Scenario: API health get_status gfhkjk34jkjlk
Given Developer Portal is opened
And I select BankCard
And I select get_status
When I click on Send this request button
Then response code should be 200
And response body should contain "STATUS"

@BankCard_get_charges
Scenario: API Charges get_charges dfgjkj43kjkj
Given Developer Portal is opened
And I select BankCard
And I select get_charges
When I click on Send this request button
Then response code should be 200
#And response body should contain "Bankcard"
And response body should contain "startDate"
And response body should contain "endDate"
And response body should contain "authCount"
And response body should contain "authTotal"
And response body should contain "saleCount"
And response body should contain "saleTotal"

@BankCard_get_charges_StartDate
Scenario: API Charges get_charges with Start date retgij5ijkjk
Given Developer Portal is opened
And I select BankCard
And I select get_charges
And I set query parameter "startDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "startDate": "2017-01-01"

@BankCard_get_charges_EndDateDate
Scenario: API Charges get_charges with End date dgkjk5jkjk4
Given Developer Portal is opened
And I select BankCard
And I select get_charges
And I set query parameter "endDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "endDate": "2017-01-01"

@BankCard_get_charges_PageSize
Scenario: API Charges get_charges With page size efgij5kjghfg
Given Developer Portal is opened
And I select BankCard
And I select get_charges
And I set query parameter "pageSize" to "10"
When I click on Send this request button
Then response code should be 200
And response body should contain "pageSize": 10

@BankCard_get_charges_Pageno
Scenario: API Charges get_charges with Page No fgjhjkh
Given Developer Portal is opened
And I select BankCard
And I select get_charges
And I set query parameter "pageNumber" to "1"
When I click on Send this request button
Then response code should be 200
And response body should contain "pageNumber": 1

@BankCard_get_charges_totalAmount
Scenario: API Charges get_charges with total amount dfgkjhjk
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"billing": {"name": "Vinayak Bhat","address": "123Main St.","city": "Reston","state": "VA","postalCode": "20190","country": "US"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
And I select BankCard
And I select get_charges
And I set query parameter "totalAmount" to "222"
When I click on Send this request button
Then response code should be 200
And response body should contain "total": 222

@BankCard_get_charges_Name
Scenario: API Charges get_charges by name dfgjhuh77jhjkh
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"billing": {"name": "Vinayak Bhat","address": "123Main St.","city": "Reston","state": "VA","postalCode": "20190","country": "US"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
And I select BankCard
And I select get_charges
And I set query parameter "name" to "Vinayak"
When I click on Send this request button
Then response code should be 200
And response body should contain "name": "Vinayak"


@BankCard_get_charges_accountNumber
Scenario: API Charges get_charges by Account No dfgjk5kjkjkjkf
Given Developer Portal is opened
And I select BankCard
And I select get_charges
And I set query parameter "accountNumber" to "1111"
When I click on Send this request button
Then response code should be 200
And response body should contain "accountNumber": "XXXXXXXXXXXX1111"

@BankCard_get_charges_refNo
Scenario: API Charges get_charges by Ref No fgkjhk5hkhjkhujh
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":45.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select get_charges
And I set query parameter "reference" to "TR_REFERENCE"
When I click on Send this request button
Then response code should be 200
And response body should contain "reference": "TR_REFERENCE"

@BankCard_get_charges_orderNo
Scenario: API Charges get_charges by Order no dfgkjh5kkhjhj
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":45.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select get_charges
And I set query parameter "orderNumber" to "TR_ORDERNO"
When I click on Send this request button
Then response code should be 200
And response body should contain "orderNumber": "TR_ORDERNO"

#******************************* post_charges***************************************************

@BankCard_post-charges_Cards
Scenario Outline: Post a sale for different Card types fgkjhk5jjjf
Given Developer Portal is opened
And I select BankCard
And I select post_charges
And I set json body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":45.0},"CardData":{"Expiration":"1122","Number":<CardNo>,"cvv":"123"}}}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "APPROVED"
And response body should contain "orderNumber"
And response body should contain "reference"

  Examples:
	|CardNo            | 
	|"4111111111111111"| 
	|"5499740000000057"|
	|"6011000993026909"|
	|"371449635392376" |
	
@BankCard_post-charges_Domains
Scenario Outline: Post a sale for different domain type dfgkjuir3fg
Given Developer Portal is opened
And I select BankCard
And I select post_charges
And I set json body to {"transactionId": "tid-1234",<Domain>: {"authorizationCode": "123456","Amounts":{"Total":46.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "APPROVED"
And response body should contain "orderNumber"
And response body should contain "reference"

  Examples:
	|Domain      | 
	|"retail"    | 
	|"eCommerce" |
	|"healthCare"|
	
@BankCard_post-charges_Types
Scenario Outline: Post a sale for different transaction type fgjhjkh5uhjkhj
Given Developer Portal is opened
And I select BankCard
And I select post_charges
And I set query parameter "type" to <type>
And I set json body to {"transactionId": "tid-1234","retail": {"authorizationCode": "123456","Amounts":{"Total":47.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "APPROVED"
And response body should contain "orderNumber"
And response body should contain "reference"

  Examples:
	|type            | 
	|"Sale"          |
	|"Authorization" |
	|"Force"         |
	
@BankCard_post-charges_AllData
Scenario: API Charges post_charges with all data dfgkjhuih4jkhjuh
Given Developer Portal is opened
And I select BankCard
And I select post_charges
And I set json body to {"transactionId": "","eCommerce": {"authorizationCode": "000001","amounts": {"total": 84.68,"tax": 2.66,"shipping": 1.06},"orderNumber": "PO# 456","cardData": {"number": "4111111111111111","expiration": "1220","cvv": "123"},"customer": {"email": "john@foo.com","telephone": "7033334444","fax": "7033334444"},"billing": {"name": "Vinayak Bhat","address": "123Main St.","city": "Reston","state": "VA","postalCode": "20190","country": "US"},"shipping": {"name": "Vinayak Bhat","address": "123Main St.","city": "Reston","state": "VA","postalCode": "20190","country": "US"},"level2": {"customerNumber": "7890"},"level3": {"destinationCountryCode": "840","amounts": {"discount": 20,"duty": 10,"nationalTax": 10},"vat": {"idNumber": "vat123456789","invoiceNumber": "PO# 456","amount": 100,"rate": 10},"customerNumber": "7890"},"isRecurring": false,"recurringSchedule": {"amount": 10,"frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "ThatDay","startDate": "2015-02-02","totalCount": 1,"groupId": "45445" }}}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "APPROVED"
And response body should contain "orderNumber"
And response body should contain "reference"

  
	
#******************************* get_charges_reference***************************************************	


@BankCard_get_charges_detail_ValidRef
Scenario: API Charges get_charges_detail with valid reference fgokjhij5jkhjkh
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":63.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select get_charges_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200
And response body should contain "reference": "TR_REFERENCE"


@BankCard_get_charges_detail_inValidRef
Scenario: API Charges get_charges details with invalid reference fgjhu7jd
Given Developer Portal is opened
And I select BankCard
And I select get_charges_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404


#******************************* put_charges***************************************************

@BankCard_put_charges_ValidRef
Scenario: API Charges put_charges_detail with valid reference dfgkjh5jhjkhjkh
Given Developer Portal is opened
And I do Authorization using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":45.85},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select put_charges
And I edit resource URL with transaction reference
And I set json body to {"amounts": {"tip": 0,"total": 10,"tax": 0,"shipping": 0}}
When I click on Send this request button
Then response code should be 200


@BankCard_put_charges_inValidRef
Scenario: API Charges put_charges with invalid reference fgkjhiuh5jhjkh
Given Developer Portal is opened
And I select BankCard
And I select put_charges
And I edit resource URL with transaction reference
And I set json body to {"amounts": {"tip": 0,"total": 10,"tax": 0,"shipping": 0}}
When I click on Send this request button
Then response code should be 404
	
	

#******************************* delete_charges***************************************************

@BankCard_delete_charges_ValidRef
Scenario: API Charges delete_charges with valid reference fgkjhjh5jhj
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":55.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select delete_charges
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200


@BankCard_delete_charges_inValidRef
Scenario: API Charges delete_charges with invalid reference dfgkjhj5hjkh
Given Developer Portal is opened
And I select BankCard
And I select delete_charges
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404


#************************************** post_charge_lineitems ******************************************
@BankCard_post_charge_lineitems_MC
Scenario: API  post_charge_lineitems with valid reference dfgkjhjh5jhj
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"5499740000000057","cvv":"123"}}}
And I select BankCard
And I select post_charge_lineitems
And I edit resource URL with transaction reference
And I set json body to {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
When I click on Send this request button
Then response code should be 201

@BankCard_post_charge_lineitems_MC_InvalidRef
Scenario: API  post_charge_lineitems with invalid reference fgkjk5kjkj
Given Developer Portal is opened
And I select BankCard
And I select post_charge_lineitems
And I edit resource URL with transaction reference
And I set json body to {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
When I click on Send this request button
Then response code should be 404


@BankCard_post_charge_lineitems_Visa
Scenario: API  post_charge_lineitems with valid reference dfghkjh
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select post_charge_lineitems
And I edit resource URL with transaction reference
And I set json body to {"visa": [{"commodityCode": "2","itemDescription": "dfgdfg","productCode": "5","quantity": 4,"unitOfMeasure": "0","unitCost": 40,"vatTaxAmount": 4,"vatTaxRate": 1,"discountAmount": 5,"lineItemTotal": 35}]}
When I click on Send this request button
Then response code should be 201

@BankCard_post_charge_lineitems_Visa_InvalidRef
Scenario: API  post_charge_lineitems with invalid reference dfgjhjkrrr
Given Developer Portal is opened
And I select BankCard
And I select post_charge_lineitems
And I edit resource URL with transaction reference
And I set json body to {"masterCard": [{"itemDescription": "Test","productCode": "Test","quantity": 1,"unitOfMeasure": "0","unitCost": 0,"taxAmount": 0,"taxRate": 0,"discountAmount": 0, "AlternateTaxIdentifier": "1","taxTypeApplied": "Test","discountIndicator": "1","netGrossIndicator": "1","extendedItemAmount": 0,    "debitCreditIndicator": "1"}]}
When I click on Send this request button
Then response code should be 404


#************************************** get_charges_lineitems_detail ******************************************

@BankCard_get_charges_lineitems_detail
Scenario: API  post_charge_lineitems with valid reference dfgkjhiujhff
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select post_charge_lineitems
And I edit resource URL with transaction reference
And I set json body to {"visa": [{"commodityCode": "2","itemDescription": "dfgdfg","productCode": "5","quantity": 4,"unitOfMeasure": "0","unitCost": 40,"vatTaxAmount": 4,"vatTaxRate": 1,"discountAmount": 5,"lineItemTotal": 35}]}
And I click on Send this request button
And I click on APISandBox
And I select BankCard
And I select get_charges_lineitems_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200
And response body should contain "commodityCode"	
And response body should contain "itemDescription"	
And response body should contain "productCode"	
And response body should contain "quantity"	
And response body should contain "unitOfMeasure"	
And response body should contain "unitCost"

@BankCard_get_charges_lineitems_detail_InvalidRef
Scenario: API  post_charge_lineitems with valid reference dfglkjiojierr
Given Developer Portal is opened
And I select BankCard
And I select get_charges_lineitems_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404


#************************************** delete_charge_lineitems ******************************************

@BankCard_delete_charge_lineitems
Scenario: API  post_charge_lineitems with valid reference dfgkjhiohjrr
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":35.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select delete_charge_lineitems
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 204

@BankCard_delete_charge_lineitems_InvalidRef
Scenario: API  post_charge_lineitems with valid reference fgkjhuijhirtr
Given Developer Portal is opened
And I select BankCard
And I select delete_charge_lineitems
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404


#************************************** CREDITS ************************************************

#******************************* post_credits***************************************************

@BankCard_post-credits_Cards
Scenario Outline: Post a credit for different Card types dfgkjioujijr
Given Developer Portal is opened
And I select BankCard
And I select post_credits
And I set json body to {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":45.0},"CardData":{"Expiration":"1122","Number":<CardNo>,"cvv":"123"}}}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "APPROVED"
And response body should contain "orderNumber"
And response body should contain "reference"

  Examples:
	|CardNo            | 
	|"4111111111111111"| 
	|"5499740000000057"|
	|"6011000993026909"|
	|"371449635392376" |


@BankCard_post-credits_Domains
Scenario Outline: Post a credit for different domain type sdfijh8iurkjfdd
Given Developer Portal is opened
And I select BankCard
And I select post_credits
And I set json body to {"transactionId": "tid-1234",<Domain>: {"authorizationCode": "123456","Amounts":{"Total":46.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "APPROVED"
And response body should contain "orderNumber"
And response body should contain "reference"

  Examples:
	|Domain      | 
	|"retail"    | 
	|"eCommerce" |
	|"healthCare"|
	
	
@BankCard_post-credits_AllData
Scenario: post a credit with alla data fdghuihjkhdd
Given Developer Portal is opened
And I select BankCard
And I select post_credits
And I set json body to {"transactionId": "","eCommerce": {"authorizationCode": "000001","amounts": {"total": 42.42,"tax": 2.12,"shipping": 1.06},"orderNumber": "PO# 456","cardData": {"number": "4111111111111111","expiration": "1220","cvv": "123"},"customer": {"email": "john@foo.com","telephone": "7033334444","fax": "7033334444"},"billing": {"name": "Vinayak Bhat","address": "123Main St.","city": "Reston","state": "VA","postalCode": "20190","country": "US"},"shipping": {"name": "Vinayak Bhat","address": "123Main St.","city": "Reston","state": "VA","postalCode": "20190","country": "US"},"level2": {"customerNumber": "7890"},"level3": {"destinationCountryCode": "840","amounts": {"discount": 20,"duty": 10,"nationalTax": 10},"vat": {"idNumber": "vat123456789","invoiceNumber": "PO# 456","amount": 100,"rate": 10},"customerNumber": "7890"},"isRecurring": false,"recurringSchedule": {"amount": 10,"frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "ThatDay","startDate": "2015-02-02","totalCount": 1,"groupId": "45445" }}}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "APPROVED"
And response body should contain "orderNumber"
And response body should contain "reference"


#******************************* get_credits***************************************************

@BankCard_get_credits
Scenario: API Charges get_charges fgjkjkjkdjkdd
Given Developer Portal is opened
And I select BankCard
And I select get_credits
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "startDate"
And response body should contain "endDate"
And response body should contain "authCount"
And response body should contain "authTotal"
And response body should contain "saleCount"
And response body should contain "saleTotal"

@BankCard_get_credits_StartDate
Scenario: API Charges get_charges dsfgkjijkjkfff
Given Developer Portal is opened
And I select BankCard
And I select get_credits
And I set query parameter "startDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "startDate": "2017-01-01"

@BankCard_get_credits_EndDateDate
Scenario: API Charges get_charges sdfgjijkjnkkkkdd
Given Developer Portal is opened
And I select BankCard
And I select get_credits
And I set query parameter "endDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "endDate": "2017-01-01"

@BankCard_get_credits_PageSize
Scenario: API Charges get_credits by Page size fdgjiujijkjf
Given Developer Portal is opened
And I select BankCard
And I select get_credits
And I set query parameter "pageSize" to "10"
When I click on Send this request button
Then response code should be 200
And response body should contain "pageSize": 10

@BankCard_get_credits_Pageno
Scenario: API Charges get_credits by Page No erjkljkjkr
Given Developer Portal is opened
And I select BankCard
And I select get_credits
And I set query parameter "pageNumber" to "1"
When I click on Send this request button
Then response code should be 200
And response body should contain "pageNumber": 1


@BankCard_get_credits_totalAmount
Scenario: API Charges get_credits with Total amount dfgjkjkjkrr
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":476},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"billing": {"name": "Vinayak Bhat","address": "123Main St.","city": "Reston","state": "VA","postalCode": "20190","country": "US"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
And I select BankCard
And I select get_credits
And I set query parameter "totalAmount" to "476"
When I click on Send this request button
Then response code should be 200
#And response body should contain "Bankcard"
And response body should contain "total": 476

@BankCard_get_ccredits_Name
Scenario: API Charges get_charges jfkgjkjkjk
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"billing": {"name": "Vinayak Bhat","address": "123Main St.","city": "Reston","state": "VA","postalCode": "20190","country": "US"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
And I select BankCard
And I select get_credits
And I set query parameter "name" to "Vinayak"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "name": "Vinayak"


@BankCard_get_credits_accountNumber
Scenario: API Charges get_charges by Account No dfgijkljkf
Given Developer Portal is opened
And I select BankCard
And I select get_credits
And I set query parameter "accountNumber" to "1111"
When I click on Send this request button
Then response code should be 200
#And response body should contain "visa"
And response body should contain "accountNumber": "XXXXXXXXXXXX1111"

@BankCard_get_credits_refNo
Scenario: API Charges get_charges by Ref No dfgjiujijkttt
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":45.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select get_credits
And I set query parameter "reference" to "TR_REFERENCE"
When I click on Send this request button
Then response code should be 200
And response body should contain "reference": "TR_REFERENCE"

@BankCard_get_credits_orderNo
Scenario: API Charges get_charges by Order no fgkjhijhk
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":45.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select get_credits
And I set query parameter "orderNumber" to "TR_ORDERNO"
When I click on Send this request button
Then response code should be 200
And response body should contain "orderNumber": "TR_ORDERNO"


#******************************* get_credits_detail***************************************************
@BankCard_get_credit_details_refNo
Scenario: API Charges get_charges by Ref No dfgkjjijkff
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":22.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select get_credits_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200
And response body should contain "reference": "TR_REFERENCE"
And response body should contain "service": "Bankcard"
And response body should contain "status": "Batch"
#And response body should contain "type": "Sale"
And response body should contain "number": "XXXXXXXXXXXX1111"

@BankCard_get_credit_details_InvalidRefNo
Scenario: API Charges get_charges by Ref No dfgjkhf
Given Developer Portal is opened
And I select BankCard
And I select get_credits_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404


#******************************* post_credits_reference***************************************************
@BankCard_post_credits_refNo
Scenario: API Charges post credits by Ref No dfgkjhiuujif
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":68},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select post_credits_reference
And I edit resource URL with transaction reference
And I set json body to {"transactionId": "","deviceId": "7894560","amount": 10,"terminalNumber": ""}
When I click on Send this request button
Then response code should be 200
And response body should contain "reference": "TR_REFERENCE"
And response body should contain "status": "Approved"
And response body should contain "message": "APPROVED"
And response body should contain "cvvResult"
And response body should contain "avsResult"
And response body should contain "riskCode"

@BankCard_post_credit_InvalidRefNo
Scenario: API Charges post credits by in valid Ref No dfgkjhkk
Given Developer Portal is opened
And I select BankCard
And I select post_credits_reference
And I edit resource URL with transaction reference
And I set json body to {"transactionId": "","deviceId": "7894560","amount": 10,"terminalNumber": ""}
When I click on Send this request button
Then response code should be 400

#******************************* delete_credits***************************************************

@BankCard_delete_credits_ValidRef
Scenario: API Charges put_charges_detail with valid reference sgfkjhuhyuj
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":22.45},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select delete_charges
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200


@BankCard_delete_credits_inValidRef
Scenario: API Charges put_charges with invalid reference sfdjjkhjj
Given Developer Portal is opened
And I select BankCard
And I select delete_charges
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404


#******************************* Reports and Transactions***************************************************

#*******************************get_transactions***************************************************
@BankCard_get_transactions
Scenario: API Charges get_transactions fjkgjkjkjkd
Given Developer Portal is opened
And I select BankCard
And I select get_transactions
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "startDate"
And response body should contain "endDate"
And response body should contain "authCount"
And response body should contain "authTotal"
And response body should contain "saleCount"
And response body should contain "saleTotal"
And response body should contain "creditCount"
And response body should contain "creditTotal"
And response body should contain "totalCount"
And response body should contain "totalVolume"

@BankCard_get_transactions_StartDate
Scenario: API Charges get_transactions Start Date dfgkjkjij4ijij
Given Developer Portal is opened
And I select BankCard
And I select get_transactions
And I set query parameter "startDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "startDate": "2017-01-01"

@BankCard_get_transactions_EndDateDate
Scenario: API Charges get_transactions by End date fgjkjkjkkdd
Given Developer Portal is opened
And I select BankCard
And I select get_transactions
And I set query parameter "endDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "endDate": "2017-01-01"

@BankCard_get_transactions_PageSize
Scenario: API Charges get_transactions by Page Size fdgkjijjfff
Given Developer Portal is opened
And I select BankCard
And I select get_transactions
And I set query parameter "pageSize" to "10"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "pageSize": 10

@BankCard_get_transactions_Pageno
Scenario: API Charges get_transactions by page no sdfiojkjnf
Given Developer Portal is opened
And I select BankCard
And I select get_transactions
And I set query parameter "pageNumber" to "5"
When I click on Send this request button
Then response code should be 200
And response body should contain "pageNumber": 5

@BankCard_get_transactions_totalAmount
Scenario: API Charges get_transactions total amount sluytrghjhnf
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":333},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"billing": {"name": "Vinayak Bhat","address": "123Main St.","city": "Reston","state": "VA","postalCode": "20190","country": "US"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
And I select BankCard
And I select get_transactions
And I set query parameter "totalAmount" to "333"
When I click on Send this request button
Then response code should be 200
#And response body should contain "Bankcard"
And response body should contain "total": 333

@BankCard_get_transactions_Name
Scenario: API Charges get_transactions by Name gjijitjijkjk
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":222.22},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"},"billing": {"name": "Vinayak Bhat","address": "123Main St.","city": "Reston","state": "VA","postalCode": "20190","country": "US"},"isRecurring": true,"recurringSchedule":{"amount":"222","frequency": "Monthly","interval": 1,"nonBusinessDaysHandling": "After","startDate": "2016-08-05","totalCount":"2"}}}
And I select BankCard
And I select get_transactions
And I set query parameter "name" to "Vinayak"
When I click on Send this request button
Then response code should be 200
And response body should contain "name": "Vinayak"


@BankCard_get_transactions_accountNumber
Scenario: API Charges get_transactions by Account No fgjijuijkjtkj
Given Developer Portal is opened
And I select BankCard
And I select get_transactions
And I set query parameter "accountNumber" to "1111"
When I click on Send this request button
Then response code should be 200
#And response body should contain "visa"
And response body should contain "accountNumber": "XXXXXXXXXXXX1111"

@BankCard_get_transactions_refNo
Scenario: API Charges get_transactions by Ref No gfjioijtrkjkjf
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":45.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select get_transactions
And I set query parameter "reference" to "TR_REFERENCE"
When I click on Send this request button
Then response code should be 200
And response body should contain "reference": "TR_REFERENCE"

@BankCard_get_transactions_orderNo
Scenario: API Charges get_transactions by Order no egiuihjjkjjhf
Given Developer Portal is opened
And I do credit using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":45.0},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select get_transactions
And I set query parameter "orderNumber" to "TR_ORDERNO"
When I click on Send this request button
Then response code should be 200
And response body should contain "orderNumber": "TR_ORDERNO"


#*******************************get_transactions_detail***************************************************
@BankCard_get_transactions_detail_ValidRef
Scenario: API Charges get_transactions_detail with valid reference ergijkjkkfkf
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":55.25},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select get_transactions_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200
And response body should contain "reference": "TR_REFERENCE"



@BankCard_get_transactions_detail_inValidRef
Scenario: API Charges post_transactions_details with invalid reference frgjkjkf
Given Developer Portal is opened
And I select BankCard
And I select get_transactions_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404		


#*******************************Settlements and Batches***************************************************
#*******************************get_batches***************************************************
@BankCard_get_batches
Scenario: API Charges get_charges egfrjiujirtijkr
Given Developer Portal is opened
And I select BankCard
And I select get_batches
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "startDate"
And response body should contain "endDate"
And response body should contain "authCount"
And response body should contain "authTotal"
And response body should contain "saleCount"
And response body should contain "saleTotal"

@BankCard_get_batches_StartDate
Scenario: API get batches frojoiolklr
Given Developer Portal is opened
And I select BankCard
And I select get_batches
And I set query parameter "startDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "startDate": "2017-01-01"

@BankCard_get_batches_EndDateDate
Scenario: API Charges get_batches with end date ergfjjijrk
Given Developer Portal is opened
And I select BankCard
And I select get_batches
And I set query parameter "endDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "endDate": "2017-01-01"

@BankCard_get_batches_PageSize
Scenario: API get_batches fgjij8ihjkk
Given Developer Portal is opened
And I select BankCard
And I select get_batches
And I set query parameter "pageSize" to "10"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "pageSize": 10

@BankCard_get_batches_Pageno
Scenario: API Charges get_batches with page no
Given Developer Portal is opened
And I select BankCard
And I select get_batches
And I set query parameter "pageNumber" to "1"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "pageNumber": 1


#*******************************get_batches_current***************************************************
@BankCard_get_batches_current_PageSize
Scenario: API Charges get_batches_current based on page size sdfkjkjk
Given Developer Portal is opened
And I select BankCard
And I select get_batches_current
And I set query parameter "pageSize" to "10"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "pageSize": 10

@BankCard_get_batches_current_Pageno
Scenario: API Charges get_batches_current based on page no sdfgkjkjkd
Given Developer Portal is opened
And I select BankCard
And I select get_batches_current
And I set query parameter "pageNumber" to "1"
When I click on Send this request button
Then response code should be 200
And response body should contain "Bankcard"
And response body should contain "pageNumber": 1


#*******************************post_batches_current***************************************************
@BankCard_post_batches_current
Scenario: API post_batches_current fghjhuhmfjk
Given Developer Portal is opened
And I select BankCard
And I select post_batches_current
And I set json body to "{"settlementType": "Bankcard"}"
When I click on Send this request button
Then response code should be 200
Given I click on APISandBox
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":33},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I select BankCard
And I select post_batches_current
And I set json body to {"settlementType": "Bankcard","count": 1,"net": 33}
When I click on Send this request button
Then response code should be 201
And response body should contain "batchNumber"
And response body should contain "count": 1
And response body should contain "batchNet": 33
And response body should contain "message": "BATCH 1 OF 1 CLOSED"
And response body should contain "batchNumber"
And response body should contain "status": "Approved"

#*******************************get_batches_current_summary***************************************************
@BankCard_get_batches_current_summary
Scenario: API get_batches_current_summary fghjhuhjf7jhjhf
Given Developer Portal is opened
And I select BankCard
And I select get_batches_current_summary
When I click on Send this request button
Then response code should be 200
And response body should contain "paymentType"
And response body should contain "authCount"
And response body should contain "authTotal"
And response body should contain "saleCount"
And response body should contain "saleTotal"
And response body should contain "creditCount"
And response body should contain "creditTotal"
And response body should contain "totalCount"
And response body should contain "totalVolume"

#*******************************get_batches_totals***************************************************
@BankCard_get_batches_totals
Scenario: API get_batches_totals dfgjhjh7jhjhfj
Given Developer Portal is opened
And I select BankCard
And I select get_batches_totals
When I click on Send this request button
Then response code should be 200
And response body should contain "count"
And response body should contain "net"
And response body should contain "volume"

#*******************************get_batches_reference***************************************************
@BankCard_get_batches_reference
Scenario: API get_batches_reference dfkgjk8jhjhjhj
Given Developer Portal is opened
And I do sale charges using body {"transactionId": "tid-1234","Retail": {"authorizationCode": "123456","Amounts":{"Total":32.25},"CardData":{"Expiration":"1122","Number":"4111111111111111","cvv":"123"}}}
And I do post batches using body {"settlementType": "Bankcard"}
And I select BankCard
And I select get_batches_reference
And I edit resource URL with batch reference
When I click on Send this request button
Then response code should be 200
And response body should contain "reference": "BCH_REFERENCE"

@BankCard_get_batches_Invalidref
Scenario: API get_batches_totals dfgljkjifkjkk
Given Developer Portal is opened
And I select BankCard
And I select get_batches_reference
And I edit resource URL with batch reference
When I click on Send this request button
Then response code should be 404



#********************************************Tokens***********************************************
#********************************************post-tokens******************************************
@Bankcard_post_token
Scenario Outline: API post token dfkgjikjfikkk
Given Developer Portal is opened
And I select Token
And I select post-tokens
And I set json body to {"cardData": {"number": <cardNo>,"expiration": "1220"}}
When I click on Send this request button
Then response code should be 200
And response body should contain "vaultResponse"
And response body should contain "status"
And response body should contain "data"
And response body should contain "message": "SUCCESS"

	Examples:
	|cardNo            | 
	|"4111111111111111"| 
	|"5499740000000057"|
	|"6011000993026909"| 
	|"371449635392376"|


#********************************************put-token******************************************
@Bankcard_put_token
Scenario: API Charges put token with valid reference dfghuhjjfjhj
Given Developer Portal is opened
And I do BankCard post_token using body {"cardData": {"number": "4111111111111111","expiration": "1220"}}
And I select Token
And I select put-token
And I edit resource URL with token
And I set json body to {"cardData": {"number": "4111111111111111","expiration": "1220"}}
When I click on Send this request button
Then response code should be 200
And response body should contain "data": "TR_TOKEN"
And response body should contain "message": "SUCCESS"


#********************************************delete-token******************************************

@Bankcard_delete_token
Scenario: API delete token with valid reference dfjgkkjkjfkkk
Given Developer Portal is opened
And I do BankCard post_token using body {"cardData": {"number": "4111111111111111","expiration": "1220"}}
And I select Token
And I select delete-token
And I edit resource URL with token
When I click on Send this request button
Then response code should be 200
And response body should contain "data": "TR_TOKEN"
And response body should contain "message": "DELETED"


@Bankcard_delete_token_invalidToken
Scenario: API delete token with in valid reference dfgjiuijkjkjkf
Given Developer Portal is opened
And I select Token
And I select delete-token
And I edit resource URL with token
When I click on Send this request button
Then response code should be 404



