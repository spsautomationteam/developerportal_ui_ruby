@Devportal
Feature: Create User Account and sign in
  I want to this template for my feature file

  @CreateAccount_ValidData_Submit
  Scenario: Verify New User Account Created Successfully with Valid data
    Given Developer Portal is opened
    When Click on 'Create Account' tab
    And Enter valid data in all required fields
    And Click on Create Account button
    Then It should create new account and should display welcome message

  @Create_Account_With_All_BlankFields
  Scenario: Verify Create Account with All Blank Fields
    Given Developer Portal is opened
    When Click on 'Create Account' tab
    And Click on Create Account button
    Then Verify response message "Username field is required."
    And Verify response message "Username field is required."
    And Verify response message "First Name field is required."
    And Verify response message "E-mail address field is required."
    And Verify response message "Last Name field is required."
    And Verify response message "Company Name field is required."
    And Verify response message "Address 1 field is required."
    And Verify response message "City field is required."
    And Verify response message "State field is required."
    And Verify response message "ZIP code field is required."
    And Verify response message "Accept Terms & Conditions of Use field is required."

  @CreateAccount_withExistingUser
  Scenario: Create account  with existing user
    Given Developer Portal is opened
    When Click on 'Create Account' tab
    And Enter valid data in all required fields
    And Click on Create Account button
    Then Verify response message "The name"
    And Verify response message "is already taken."
    And Verify response message "The e-mail address"
    And Verify response message "is already registered. Have you forgotten your password?"


  @User_SignIn_With_ValidData_And_LogOut
  Scenario: Verify user able to login into 'Sage Developer Portal' with valid login credentials
    Given Developer Portal is opened
    When Click on 'tabSignIn' Tab present at top of the Page
    And I enter Developer Portal username "vinayaksbhat"
    And I enter Developer Portal password "Sage1234"
    And Click on Signin button
    Then Verify response message "My Apps"
    And Verify response message "My Profile"

  @User_SignIn_With_Invalid_Data
  Scenario: Verify user able to login into 'Sage Developer Portal' with in valid login credentials
    Given Developer Portal is opened
    When Click on 'tabSignIn' Tab present at top of the Page
    And I enter Developer Portal username "dsdsddddddd"
    And I enter Developer Portal password "dddddsss"
    And Click on Signin button
    Then Verify response message "Sorry, unrecognized username or password. Have you forgotten your password?"


  @User_SignIn_With_All_BlankFields
  Scenario: Verify User_SignIn with All Blank Fields
    Given Developer Portal is opened
    When Click on 'tabSignIn' Tab present at top of the Page
    And I enter Developer Portal username ""
    And I enter Developer Portal password ""
    And Click on Signin button
    Then Verify response message "User Name or e-mail address field is required."
    And Verify response message "Password field is required."