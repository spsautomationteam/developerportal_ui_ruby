@Devportal
Feature: Verify All Features under Support Page/Tab
  I want to this template for my feature file

  @Validate_AllOptions_Under_Support_Page_WithOut_SignIn
  Scenario: Verify displaying 'controls'at Support page
    Given Developer Portal is opened
    When Click on 'tabSupport' Tab present at top of the Page
    Then Verify It should display the required options under 'Support' page

  @Validate_SubmitWithOutData
  Scenario: Verify submit with out any data
    Given Developer Portal is opened
    When Click on 'tabSupport' Tab present at top of the Page
    And Click on SUBMIT button
    Then Verify tool tip 'Please fill out this field'


  @Validate_SubmitWithValidData
  Scenario: Verify submit with valid data
    Given Developer Portal is opened
    When Click on 'tabSupport' Tab present at top of the Page
    And Enter valid email "vinayak.bhat@sage.com"
    And Enter description "aaaaaa bbbbbbb cccc dddd fff"
    And Click on SUBMIT button
    Then Verify response message "Thank you, your submission has been received."

  @Validate_GoBackToForm
  Scenario: Click on Go back to form link and verify
    Given Developer Portal is opened
    When Click on 'tabSupport' Tab present at top of the Page
    And Enter valid email "abcdefg@gmail.com"
    And Enter description "aggjjkkrrr"
    And Click on SUBMIT button
    Then Verify response message "Thank you, your submission has been received."
    When Click on 'Go back to the form' link
    Then Verify It should display the required options under 'Support' page

  @Validate_ExceededCharsInDescription
  Scenario: Verify more message by entering more than 500 char in description
    Given Developer Portal is opened
    When Click on 'tabSupport' Tab present at top of the Page
    And Enter valid email "abcdefg@gmail.com"
    And Enter description "asdflhiuergnvurehg;agjerjgjfjdfgjjkdf dfkjkjkjfdg lkjdfg troijifgkjrtguihjfgu8947589rujdkf894utihjjfg9834u5tuiodfgj89u55t98u54uierjfg9843u589tgu98jefg9v8u8945utgtr8*&*&jk fgbkjk fgkkfjkgkjdfgoijjejgjiojfgjiojtrgoijfjgiojiojgijiotjgiojiotjgoijoitgj;dojgoijd;fgjdofjgoijljdfkjgkljdkfjgkjkdfjgkjkdjfkgjkjdfkgjkjkdlfjgkljkjdkfjgijijfkgjkjdkfjgi dfjkj uhrejnfuhurjnjnfvujnrefjnknkjsdnfjhjnjnfjnjn f fjnhnrgiujdiiuhql;qqnxihjjhj juhuifjkvvoirjijfv984uuj4398uujijijvc8934ujfj34cv98j34vm34jvcjpsdfvj8934u9fjodkvj98j498fjiorjvuj9834j9ijvj894j98fuefvioj43598uiufj*(&**(uijieru89jekjjhfgkikijn"
    Then Verify response message "Content limited to 500 characters, remaining: 0"
