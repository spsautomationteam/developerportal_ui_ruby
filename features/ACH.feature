@DevPortal
Feature: ACH
	I want to use this template for my feature file
	
#Background:
#	Given Developer Portal is opened

@ACH_get_ping
Scenario: API health get_ping
Given Developer Portal is opened
Given I select ach
And I select get_ping
When I click on Send this request button
Then response code should be 200
And response body should contain "PONG"

@ACH_get_status
Scenario: API health get_status
Given Developer Portal is opened
And I select ach
And I select get_status
When I click on Send this request button
Then response code should be 200
And response body should contain "STATUS"

@ACH_get_charges
Scenario: API Charges get_charges All
Given Developer Portal is opened
And I select ach
And I select get_charges
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "startDate"
And response body should contain "endDate"
And response body should contain "authCount"
And response body should contain "authTotal"
And response body should contain "saleCount"
And response body should contain "saleTotal"

@ACH_get_charges_StartDate
Scenario: API Charges get_charges by Start date dfgjkjdfikk
Given Developer Portal is opened
And I select ach
And I select get_charges
And I set query parameter "startDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
#And response body should contain "ACH"
#And response body should contain "startDate"
And response body should contain "startDate": "2017-01-01"

@ACH_get_charges_EndDateDate
Scenario: API Charges get_charges by End Date dfdtdffdggg
Given Developer Portal is opened
And I select ach
And I select get_charges
And I set query parameter "endDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "endDate": "2017-01-01"

@ACH_get_charges_PageSize
Scenario: API Charges get_charges by page size gggggfff
Given Developer Portal is opened
And I select ach
And I select get_charges
And I set query parameter "pageSize" to "10"
When I click on Send this request button
Then response code should be 200
#And response body should contain "ACH"
And response body should contain "pageSize": 10

@ACH_get_charges_Pageno
Scenario: API Charges get_charges by page no ddddddddd
Given Developer Portal is opened
And I select ach
And I select get_charges
And I set query parameter "pageNumber" to "1"
When I click on Send this request button
Then response code should be 200
#And response body should contain "ACH"
And response body should contain "pageNumber": 1

@ACH_get_charges_Name
Scenario: API Charges get_charges by Name dfgjkjd
Given Developer Portal is opened
And I select ach
And I select get_charges
And I set query parameter "name" to "test FirstName middle name last name"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "name": "test FirstName middle name last name"


@ACH_get_charges_accountNumber
Scenario: API Charges get_charges by account no ttttttt
Given Developer Portal is opened
And I select ach
And I select get_charges
And I set query parameter "accountNumber" to "1234"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "accountNumber": "XXXXXXXXXX1234"

@ACH_get_charges_refNo
Scenario: API Charges get_charges by Reference No hhhhhh
Given Developer Portal is opened
And I do ACH sale using body {"transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345"}}
And I select ach
And I select get_charges
And I set query parameter "reference" to "TR_REFERENCE"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "reference": "TR_REFERENCE"


@ACH_get_charges_orderNo
Scenario: API Charges get_charges by Order No dfgrthfgh
Given Developer Portal is opened
And I do ACH sale using body { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_charges
And I set query parameter "orderNumber" to "TR_ORDERNO"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "orderNumber": "TR_ORDERNO"

#**********************************post_charges************************************************

@ACH_post-charges
Scenario Outline: Post a sale for different Transaction class and account type sdfgiuhd
Given Developer Portal is opened
And I select ach
And I select post_charges
And I set body to { "transactionClass": <TxClass>, "amounts":{ "total": 1.0 }, "account":{ "type": <accountType>, "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "orderNumber"

	Examples:
	|TxClass|accountType|
#	|"ARC"|"Savings"|
#	|"ARC"|"Checking"|
	|"CCD"|"Savings"|
#	|"CCD"|"Checking"|
#	|"PPD"|"Savings"|
	|"PPD"|"Checking"|
#	|"RCK"|"Savings"|
#	|"RCK"|"Checking"|
#	|"TEL"|"Savings"|
#	|"TEL"|"Checking"|
#	|"WEB"|"Savings"|
#	|"WEB"|"Checking"|
	
@ACH_post-charges-AllData
Scenario: Post a sale for all data dsfjgiuertjgkg
Given Developer Portal is opened
And I select ach
And I select post_charges
And I set body to {"deviceId": "123","secCode": "PPD","originatorId": "12345","amounts": {"total": 65,"tax": 10,"shipping": 10},"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"},"customer": {"dateOfBirth": "2017-01-01","ssn": "123123123","license": {"number": "12314515","stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com","telephone": "001111111111","fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name","last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "d4749cec5fb94cf38ee971cfe34d0af2",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "orderNumber"

		
@ACH_post-charges-withVaultCreate
Scenario: Post a sale for withVaultCreate dfglkjld
Given Developer Portal is opened
And I select ach
And I select post_charges
And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {  "operation": "Create"     }}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "orderNumber"
And response body should contain "vaultResponse"
And response body should contain "data"

@ACH_post-charges-withVaultRead
Scenario: Post a sale for withVaultread sdfkhjiuhjsdj
Given Developer Portal is opened
And I select ach
And I select post_charges
And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {         "token": "d4749cec5fb94cf38ee971cfe34d0af2",         "operation": "Read"     }}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "orderNumber"
And response body should contain "reference"

@ACH_post-charges-withVaultUpdate
Scenario: Post a sale for withVaultUpdate dsfghuiyhj
Given Developer Portal is opened
And I select ach
And I select post_charges
And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {         "token": "d4749cec5fb94cf38ee971cfe34d0af2",         "operation": "Update"     }}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "orderNumber"
And response body should contain "reference"


#**********************************get_charges_detail************************************************	
@ACH_get_charges_detail
Scenario: API Charges get_charges by Reference No rtrtrtrtrtrt
Given Developer Portal is opened
And I do ACH sale using body { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_charges_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "reference": "TR_REFERENCE"
And response body should contain "amounts"

@ACH_get_charges_detail_invalidRef
Scenario: API Charges get_charges by Reference No dfgjkhuihjrnj
Given Developer Portal is opened
And I select ach
And I select get_charges_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404

#******************************* delete_charges***************************************************

@ACH_delete_charges_ValidRef
Scenario: API Charges put_charges_detail with valid reference sdflkjiojtkjj
Given Developer Portal is opened
And I do ACH sale using body "{ "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }"
And I select ach
And I select delete_charges
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200


@ACH_delete_charges_inValidRef
Scenario: API Charges put_charges with invalid reference jkhjsdfgjhj
Given Developer Portal is opened
And I select ach
And I select delete_charges
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404

#******************************* Credits***************************************************	
#*******************************get_credits***************************************************	

@ACH_get_credits
Scenario: API get_credits dfglkjkjkk
Given Developer Portal is opened
And I select ach
And I select get_credits
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "startDate"
And response body should contain "endDate"
And response body should contain "authCount"
And response body should contain "authTotal"
And response body should contain "saleCount"
And response body should contain "saleTotal"

@ACH_get_credits_StartDate
Scenario: API get_dredits by start date kjhkhdsfghujrj
Given Developer Portal is opened
And I select ach
And I select get_credits
And I set query parameter "startDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "startDate"
And response body should contain "startDate": "2017-01-01"

@ACH_get_credits_EndDateDate
Scenario: API get_dredits by End date jfkgjkjf
Given Developer Portal is opened
And I select ach
And I select get_credits
And I set query parameter "endDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "endDate": "2017-01-01"

@ACH_get_credits_PageSize
Scenario: API get_credits by page size ttttttttt
Given Developer Portal is opened
And I select ach
And I select get_credits
And I set query parameter "pageSize" to "10"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "pageSize": 10

@ACH_get_credits_Pageno
Scenario: API get_charges by page no dlkfjgkljitk
Given Developer Portal is opened
And I select ach
And I select get_credits
And I set query parameter "pageNumber" to "2"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "pageNumber": 2

@ACH_get_credits_Name
Scenario: API get_charges by Name dfgjijuijrkj
Given Developer Portal is opened
And I do ACH credit using body { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "Vinayak", "last": "Bhat" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_credits
And I set query parameter "name" to "test FirstName middle name last name"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "name": "test FirstName middle name last name"


@ACH_get_credits_accountNumber
Scenario: API get_credits by Account No dfjgiojk
Given Developer Portal is opened
And I select ach
And I select get_credits
And I set query parameter "accountNumber" to "1234"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "accountNumber": "XXXXXXXXXX1234"

@ACH_get_credits_refNo
Scenario: API Charges get_credits by Reference No ddddddddd
Given Developer Portal is opened
And I do ACH credit using body { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_credits
And I set query parameter "reference" to "TR_REFERENCE"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "reference": "TR_REFERENCE"


@ACH_get_credits_orderNo
Scenario: API Charges get_credits by Order No sdfkgjhjkhj
Given Developer Portal is opened
And I do ACH credit using body { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_credits
And I set query parameter "orderNumber" to "TR_ORDERNO"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "orderNumber": "TR_ORDERNO"

#**********************************post_credits************************************************

@ACH_post-credits
Scenario Outline: Post a credits for different Transaction class and account type 55555
Given Developer Portal is opened
And I select ach
And I select post_credits
And I set body to { "transactionClass": <TxClass>, "amounts":{ "total": 1.0 }, "account":{ "type": <accountType>, "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "orderNumber"

	Examples:
	|TxClass|accountType|
#	|"ARC"|"Savings"|
#	|"ARC"|"Checking"|
	|"CCD"|"Savings"|
	|"CCD"|"Checking"|
#	|"PPD"|"Savings"|
#	|"PPD"|"Checking"|
#	|"RCK"|"Savings"|
#	|"RCK"|"Checking"|
#	|"TEL"|"Savings"|
#	|"TEL"|"Checking"|
#	|"WEB"|"Savings"|
#	|"WEB"|"Checking"|
	
@ACH_post-credits-AllData
Scenario: Post a credits for All data fghjkhjf
Given Developer Portal is opened
And I select ach
And I select post_credits
And I set body to {"deviceId": "123","secCode": "PPD","originatorId": "12345","amounts": {"total": 10,"tax": 10,"shipping": 10},"account": {"type": "Checking","routingNumber": "056008849","accountNumber": "12345678901234"},"customer": {"dateOfBirth": "2017-01-01","ssn": "123123123","license": {"number": "12314515","stateCode": "VA"         },         "ein": "123456789",         "email": "test@gmail.com","telephone": "001111111111","fax": "00111111"     },     "billing": {         "name": {             "first": "test FirstName",             "middle": "middle name","last": "last name",             "suffix": "S"         },         "address": "Restong",         "city": "Restong",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "shipping": {         "name": "SName",         "address": "SAddress",         "city": "SCity",         "state": "Virginia",         "postalCode": "24011",         "country": "USA"     },     "orderNumber": "987654",     "isRecurring": true,         "recurringSchedule": { "amount": 1,"frequency": "Monthly", "interval": 3, "nonBusinessDaysHandling": "before", "startDate": "2017-04-10","totalCount": 13,"groupId": "" },     "vault": {         "token": "d4749cec5fb94cf38ee971cfe34d0af2",         "operation": "Read"     }, 	 "memo": "This is an automation test generated Tx" }
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "orderNumber"

		
@ACH_post-credits-withVaultCreate
Scenario: Post a credit for withVaultCreate dfgjkljll
Given Developer Portal is opened
And I select ach
And I select post_credits
And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {  "operation": "Create"     }}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "orderNumber"
And response body should contain "vaultResponse"
And response body should contain "data"

@ACH_post-credits-withVaultRead
Scenario: Post a sale for withVaultCreate sdfjkhuiej
Given Developer Portal is opened
And I select ach
And I select post_credits
And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {         "token": "d4749cec5fb94cf38ee971cfe34d0af2",         "operation": "Read"     }}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "orderNumber"
And response body should contain "reference"

@ACH_post-credits-withVaultUpdate
Scenario: Post a credit for withVaultCreate kjhsdfhuhj
Given Developer Portal is opened
And I select ach
And I select post_credits
And I set body to { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" },"vault": {         "token": "d4749cec5fb94cf38ee971cfe34d0af2",         "operation": "Update"     }}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "orderNumber"
And response body should contain "reference"


#**********************************get_credits_detail************************************************	
@ACH_get_charges_detail
Scenario: API Charges get_charges by Reference No dflgkjklj
Given Developer Portal is opened
And I do ACH credit using body { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_credits_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "reference": "TR_REFERENCE"
And response body should contain "amounts"

@ACH_get_charges_detail_invalidRef
Scenario: API Charges get_charges by in valid Reference No gyghgyghgy67ghh
Given Developer Portal is opened
And I select ach
And I select get_credits_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404

#******************************* delete_credits***************************************************

@ACH_delete_credits_ValidRef
Scenario: API Charges delete_charges_detail with valid reference sfuir77rui
Given Developer Portal is opened
And I do ACH credit using body { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select delete_credits
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200


@ACH_delete_credits_inValidRef
Scenario: API Charges put_charges with invalid reference gkljiw8ikjkdjf
Given Developer Portal is opened
And I select ach
And I select delete_credits
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404

#*******************************post_credits_reference***************************************************
@ACH_post_credits_ValidRef
Scenario: API post credits with valid reference dfglkj85jk
Given Developer Portal is opened
And I do ACH sale using body { "transactionClass": "PPD", "amounts":{ "total": 1.0 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select post_credits_reference
And I edit resource URL with transaction reference
And I set body to {"amount": 1}
When I click on Send this request button
Then response code should be 201
And response body should contain "status": "Approved"
And response body should contain "message": "ACCEPTED"
And response body should contain "reference"
And response body should contain "orderNumber"


@ACH_post_credits_inValidRef
Scenario: API Charges put_charges_detail with in valid reference dfkglji34ijklf
Given Developer Portal is opened
And I select ach
And I select post_credits_reference
And I edit resource URL with transaction reference
And I set body to {"amount": 1}
When I click on Send this request button
Then response code should be 400
And response body should contain "INVALID T_REFERENCE"


#*******************************Reporting on Transactions***************************************************

#*******************************get_transactions************************************************************
@ACH_get_transactions
Scenario: API Charges get_transactions kjsdfhjkhdfjj
Given Developer Portal is opened
And I select ach
And I select get_transactions
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "startDate"
And response body should contain "endDate"
And response body should contain "authCount"
And response body should contain "authTotal"
And response body should contain "saleCount"
And response body should contain "saleTotal"
And response body should contain "creditCount"
And response body should contain "creditTotal"
And response body should contain "totalCount"
And response body should contain "totalVolume"

@ACH_get_transactions_StartDate
Scenario: API Charges get_transactions Start Date fdgjkr87hjkhf
Given Developer Portal is opened
And I select ach
And I select get_transactions
And I set query parameter "startDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "startDate": "2017-01-01"

@ACH_get_transactions_EndDateDate
Scenario: API Charges get_transactions by End date dfglkj85u4jkfk
Given Developer Portal is opened
And I select ach
And I select get_transactions
And I set query parameter "endDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "endDate": "2017-01-01"

@ACH_get_transactions_PageSize
Scenario: API Charges get_transactions by Page Size dfglkjkjijjrjk
Given Developer Portal is opened
And I select ach
And I select get_transactions
And I set query parameter "pageSize" to "10"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "pageSize": 10

@ACH_get_transactions_Pageno
Scenario: API Charges get_transactions by page no slhiuerh87yhjf87
Given Developer Portal is opened
And I select ach
And I select get_transactions
And I set query parameter "pageNumber" to "1"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "pageNumber": 1

@ACH_get_transactions_totalAmount
Scenario: API Charges get_transactions total amount eriuj89ujk45jkjk
Given Developer Portal is opened
And I do ACH sale using body { "transactionClass": "PPD", "amounts":{ "total": 222 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "jean-luc", "last": "picard" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_transactions
And I set query parameter "totalAmount" to "222"
When I click on Send this request button
Then response code should be 200
#And response body should contain "Bankcard"
And response body should contain "total": 222

@ACH_get_transactions_Name
Scenario: API Charges get_transactions by Name dfjgkh78yh5jhjhfj
Given Developer Portal is opened
And I do ACH sale using body { "transactionClass": "PPD", "amounts":{ "total": 222.22 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "Vinayak", "last": "bhat" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_transactions
And I set query parameter "name" to "Vinayak"
When I click on Send this request button
Then response code should be 200
And response body should contain "name": "Vinayak"


@ACH_get_transactions_accountNumber
Scenario: API Charges get_transactions by Account No dgrfoijuihr8hjhj
Given Developer Portal is opened
And I select ach
And I select get_transactions
And I set query parameter "accountNumber" to "1234"
When I click on Send this request button
Then response code should be 200
#And response body should contain "visa"
And response body should contain "accountNumber": "XXXXXXXXXX1234"

@ACH_get_transactions_refNo
Scenario: API Charges get_transactions by Ref No dfgiuhy87yu45hu
Given Developer Portal is opened
And I do ACH sale using body { "transactionClass": "PPD", "amounts":{ "total": 1 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "Vinayak", "last": "bhat" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_transactions
And I set query parameter "reference" to "TR_REFERENCE"
When I click on Send this request button
Then response code should be 200
And response body should contain "reference": "TR_REFERENCE"

@ACH_get_transactions_orderNo
Scenario: API Charges get_transactions by Order no fgrjh87h5jhjhf
Given Developer Portal is opened
And I do ACH sale using body { "transactionClass": "PPD", "amounts":{ "total": 1 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "Vinayak", "last": "bhat" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_transactions
And I set query parameter "orderNumber" to "TR_ORDERNO"
When I click on Send this request button
Then response code should be 200
And response body should contain "orderNumber": "TR_ORDERNO"


#*******************************get_transactions_detail************************************************************
@ACH_get_transactions_detail_ValidRef
Scenario: API Charges get_transactions_detail with valid reference rgjkh84ukjhkdfjijk
Given Developer Portal is opened
And I do ACH sale using body { "transactionClass": "PPD", "amounts":{ "total": 1 }, "account":{ "type": "Checking", "routingNumber": "056008849", "accountNumber": "12345678901234" }, "customer": { "dateOfBirth": "1998-01-01", "ssn": "123456789","license": {"number": "1234567","stateCode": "VA"},"ein": "123456789", "email": "test@gmail.com","telephone": "001111111111", "fax": "001111111"  },"billing": { "name": { "first": "Vinayak", "last": "bhat" }, "address": "123 Street Rd", "city": "cityville", "state": "va", "postalCode": "12345" } }
And I select ach
And I select get_transactions_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 200
And response body should contain "reference": "TR_REFERENCE"



@ACH_get_transactions_detail_inValidRef
Scenario: API Charges post_transactions_details with invalid reference ergkj8j4k5hk
Given Developer Portal is opened
And I select ach
And I select get_transactions_detail
And I edit resource URL with transaction reference
When I click on Send this request button
Then response code should be 404	


#*******************************Settlements and Batches************************************************************

#********************************************get_batches***********************************************************

@ACH_get_batches
Scenario: API Charges get_batches ergfjk8kjfjk
Given Developer Portal is opened
And I select ach
And I select get_batches
When I click on Send this request button
Then response code should be 200
And response body should contain "service": "ACH"
And response body should contain "startDate"
And response body should contain "endDate"
And response body should contain "count"
And response body should contain "net"
And response body should contain "volume"

@ACH_get_batches_StartDate
Scenario: API Charges get_transactions Start Date ergihj845jkk
Given Developer Portal is opened
And I select ach
And I select get_batches
And I set query parameter "startDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "startDate": "2017-01-01"

@ACH_get_batches_EndDateDate
Scenario: API Charges get_transactions by End date fgijh8jrkjk
Given Developer Portal is opened
And I select ach
And I select get_batches
And I set query parameter "endDate" to "2017-01-01"
When I click on Send this request button
Then response code should be 200
And response body should contain "endDate": "2017-01-01"

@ACH_get_batches_PageSize
Scenario: API Charges get_transactions by Page Size gfjhuh8ijhk
Given Developer Portal is opened
And I select ach
And I select get_batches
And I set query parameter "pageSize" to "10"
When I click on Send this request button
Then response code should be 200
And response body should contain "pageSize": 10

@ACH_get_batches_Pageno
Scenario: API Charges get_transactions by page no rfuih34uhjhjf
Given Developer Portal is opened
And I select ach
And I select get_batches
And I set query parameter "pageNumber" to "1"
When I click on Send this request button
Then response code should be 200
And response body should contain "pageNumber": 1

#********************************************get_batches_current*****************************************

@ACH_get_batches_current_PageSize
Scenario: API Charges get_transactions by Page Size fergiji43jk
Given Developer Portal is opened
And I select ach
And I select get_batches_current
And I set query parameter "pageSize" to "10"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "pageSize": 10

@ACH_get_batches_current_Pageno
Scenario: API Charges get_transactions by page no ertihjiuhjer
Given Developer Portal is opened
And I select ach
And I select get_batches_current
And I set query parameter "pageNumber" to "1"
When I click on Send this request button
Then response code should be 200
And response body should contain "ACH"
And response body should contain "pageNumber": "1"

#********************************************get_batches_current_summary************************************

@ACH_get_batches_current_summary
Scenario: API Charges get_batches_current_summary sdfhui4jnjhjd
Given Developer Portal is opened
And I select ach
And I select get_batches_current_summary
When I click on Send this request button
Then response code should be 200
And response body should contain "paymentType": "ARC"
And response body should contain "paymentType": "CCD"
And response body should contain "paymentType": "PPD"
And response body should contain "paymentType": "RCK"
And response body should contain "paymentType": "TEL"
And response body should contain "paymentType": "WEB"

#********************************************get_batches_totals************************************

@ACH_get_batches_totals
Scenario: API Charges get_batches_totals fdkjuihj87jk8er
Given Developer Portal is opened
And I select ach
And I select get_batches_totals
When I click on Send this request button
Then response code should be 200
And response body should contain "count"
And response body should contain "net"
And response body should contain "volume"


#********************************************Tokens***********************************************
#********************************************post-tokens******************************************
@ACH_post_token
Scenario Outline: API post token rgfkjij8kjnjkn
Given Developer Portal is opened
And I select ach
And I select post-tokens
And I set body to {"account": {"type": <accountType>,"routingNumber": <routingNumber>,"accountNumber": <accountNo>}}
When I click on Send this request button
Then response code should be 200
And response body should contain "vaultResponse"
And response body should contain "status"
And response body should contain "data"
And response body should contain "message"

	Examples:
	|accountType| routingNumber   |   accountNo     |
	|"Savings"  | "056008849"     | "12345678901234"|
	|"Checking" | "056008849"     | "12345678901234"|   



#********************************************put-token******************************************
@ACH_put_token
Scenario: API Put token dfghjh3hjhhjfhu
Given Developer Portal is opened
And I do ACH post_token using body {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
And I select ach
And I select put-token
And I edit resource URL with token
And I set body to {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
When I click on Send this request button
Then response code should be 200
And response body should contain "data": "TR_TOKEN"
And response body should contain "message": "SUCCESS"


#********************************************delete-token******************************************

@ACH_delete_token
Scenario: API delete token with valid token dfgkj8uhjdfhjkh
Given Developer Portal is opened
And I do ACH post_token using body {"account": {"type": "Savings","routingNumber": "056008849","accountNumber": "12345678901234"}}
And I select ach
And I select delete-token
And I edit resource URL with token
When I click on Send this request button
Then response code should be 200
And response body should contain "data": "TR_TOKEN"
And response body should contain "message": "DELETED"


@ACH_delete_token_invalidToken
Scenario: API API delete token with in valid token dfgkju78yuhjfdhj
Given Developer Portal is opened
And I select ach
And I select delete-token
And I edit resource URL with token
When I click on Send this request button
Then response code should be 404






