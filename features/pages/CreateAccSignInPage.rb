class CreateAccSignInPage

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'watir'

  def initialize(browser)
    @browser = browser


    @DPHomeTab    = @browser.a(:xpath => "//a[text()='ACH']")
    @panelAchGetPing = @browser.a(:xpath => "//a[text()='get_ping']")

    @tabHome = @browser.a(:xpath => "//a[contains(text(),'Home']")
    @tabApiSandBox = @browser.a(:xpath =>"//a[contains(text(),'API Sandbox']")
    @tabDocumentation = @browser.a(:xpath =>"//a[text()='Documentation']")
    @tabSupport = @browser.a(:xpath =>"//a[text()='Support']")

    @txtFirstName = @browser.text_field(:id => "edit-field-first-name-und-0-value")
    @txtLastName = @browser.text_field(:id => "edit-field-last-name-und-0-value")
    @txtUserName = @browser.text_field(:id => "edit-name")
    @txtEmail = @browser.text_field(:id => "edit-mail")
    @txtCompanyName = @browser.text_field(:id => "edit-field-company-name-und-0-value")
    @selectCountry = @browser.select_list(:name => "field_business_address[und][0][country]")
    @txtAddress1 = @browser.text_field(:name => "field_business_address[und][0][thoroughfare]")
    @txtAddress2 = @browser.text_field(:name => "field_business_address[und][0][premise]")
    @txtCity = @browser.text_field(:name => "field_business_address[und][0][locality]")
    @selectState = @browser.select_list(:name => "field_business_address[und][0][administrative_area]")
    @txtZipCode = @browser.text_field(:name => "field_business_address[und][0][postal_code]")
    @chkTermsCondition = @browser.checkbox(:name => "legal_accept")
    @btnCreateAccount = @browser.button(:id => "edit-submit")
    @btnSignIn = @browser.button(:id => "edit-submit")

    # Sign in
    @txtUserName = @browser.text_field(:name => "name")
    @txtPassword = @browser.text_field(:name => "pass")
    #@btnSignIn = @browser.button(:id => "edit-submit")

    @data = YAML.load_file('Data.yaml')
  end


  def verifyTextInPage(text)
    expect(@browser.text).to include(text)
  end

  def editFirstName(text)
    @txtFirstName.set(text)
  end

  def editLastName(text)
    @txtLastName.set(text)
  end

  def editUserName(text)
    @txtUserName.set(text)
  end

  def editEmail(text)
    @txtEmail.set(text)
  end

  def editCompanyName(text)
    @txtCompanyName.set(text)
  end

  def selectCountry(text)
    sleep 2
    @selectCountry.select (text)
    sleep 2
  end

  def editAddress1(text)
    @txtAddress1.set (text)
  end

  def editAddress2(text)
    @txtAddress2.set (text)
  end

  def editCity(text)
    @txtCity.set (text)
  end

  def selectState(text)
    sleep 2
    @selectState.select (text)
    sleep 2
  end

  def editZipCode(text)
    @txtZipCode.set (text)
  end

  def chkTermsCondition
    @chkTermsCondition.set
  end

  def clickCreateAccountBtn
    @btnCreateAccount.click
  end

  def setUserName(strUserName)
    @txtUserName.set strUserName
  end

  def setPassword(strPassword)
    @txtPassword.set strPassword
  end

  def clickSignIn
    @btnSignIn.click
    sleep 5
  end



end