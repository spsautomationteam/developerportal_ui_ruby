require 'rubygems'
require 'rspec'
require 'rspec/expectations'
require 'rspec/core'
require 'rspec/collection_matchers'
require 'watir'
require 'net/http'

include RSpec::Expectations
$TR_REFERENCE = "RRRRRRR"
$TR_ORDERNO = "OOOOOOO"
$TR_TOKEN = "TTTTTT"

class ACHPage
  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'watir'


  attr_accessor :loginTab,:txtUsername,:txtPassword,:btnLogin

  def initialize(browser)
    @browser = browser
    @DPHomeTab    = @browser.a(:xpath => "//a[text()='ACH']")
    @panelAchGetPing = @browser.a(:xpath => "//a[text()='get_ping']")
    #@panelAchGetPing = @browser.div(:xpath => ".//*[@id='block-system-main']/div[2]/div/div[2]/div[2]/div/span/div")

    @panelAch = @browser.a(:xpath => "//a[text()='ACH']")
    @panelAchGetStatus = @browser.a(:xpath => "//a[text()='get_status']")
    @btnSendThisRequest = @browser.button(:xpath => "//button[text()='Send this request']")
    @panelResponseCode = @browser.strong(:xpath => ".//*[@id='request_response_container']/div[1]/strong")
    #@panelResponseMessage = @browser.pre(:xpath => ".//*[@id='request_response_container']/div[1]/pre")
    #@panelResponseMessage = @browser.code(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[6]/div[1]/pre/code")
    @panelResponseMessage = @browser.code(:id => "some-code123")




    @panelGetCharges = @browser.a(:xpath => "//a[text()='get_charges']")
    @panelPostCharges = @browser.a(:xpath => "//a[text()='post_charges']")
    @panelGetChargesDetails =  @browser.a(:xpath => "//a[text()='get_charges_detail']")
    @panelPutCharges = @browser.a(:xpath => "//a[text()='put_charges']")
    @panelDeleteCharges = @browser.a(:xpath => "//a[text()='delete_charges']")
    @panelPostChargesLineitems = @browser.a(:xpath => "//a[text()='post_charge_lineitems']")
    @panelGetChargesLineitems = @browser.a(:xpath => "//a[text()='get_charges_lineitems_detail']")
    @panelDeleteChargeLineitems = @browser.a(:xpath => "//a[text()='delete_charge_lineitems']")
    @panelPostCredits = @browser.a(:xpath => "//a[text()='post_credits']")
    @panelGetCredits = @browser.a(:xpath => "//a[text()='get_credits']")
    @panelGetCreditsDetails = @browser.a(:xpath => "//a[text()='get_credits_detail']")
    @panelPostCreditsReference = @browser.a(:xpath => "//a[text()='post_credits_reference']")
    @panelDeleteCredits = @browser.a(:xpath => "//a[text()='delete_credits']")
    @panelGetTransactions = @browser.a(:xpath => "//a[text()='get_transactions']")
    @panelGetTransactionsDetail = @browser.a(:xpath => "//a[text()='get_transactions_detail']")
    @panelGetBatches = @browser.a(:xpath => "//a[text()='get_batches']")
    @panelGetBatchesCurrent = @browser.a(:xpath => "//a[text()='get_batches_current']")
    @panelPostBatchesCurrent = @browser.a(:xpath => "//a[text()='post_batches_current']")
    @panelGetBatchesCurrentSummary = @browser.a(:xpath => "//a[text()='get_batches_current_summary']")
    @panelGetBatchesTotals = @browser.a(:xpath => "//a[text()='get_batches_totals']")
    @panelGetBatchesReference = @browser.a(:xpath => "//a[text()='get_batches_reference']")

    @panelPostToken = @browser.a(:xpath => "//a[text()='post-tokens']")
    @panelPutToken = @browser.a(:xpath => "//a[text()='put-token']")
    @panelDeleteToken = @browser.a(:xpath => "//a[text()='delete-token']")

    #@panelPostChargesBody = @browser.a(:xpath => ".//*[@id='request_payload']/div/div/div[2]/div/div[5]/div/div[1]/div/div/div/div[3]/div[1]")
    @panelPostChargesBody = @browser.pre(:xpath => ".//*[@id='request_payload']/div/div/div[2]/div/div[5]/div/div[1]/div/div/div/div[3]/div[1]/pre")
    #@panelPostChargesBody = @browser.div(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[4]/div[2]/div/div/div[2]/div/div[5]/div/div[1]/div/div/div/div[3]")

    @spanReference = @browser.a(:xpath => ".//*[@id='method_url_panel']/div/p/span[2]/span/span[1]")
    @panelQueryParameter = @browser.a(:xpath => ".//*[@id='accordion']/div[2]/div[1]/a")
    @panelResourceUrl = @browser.a(:xpath => ".//*[@id='accordion']/div[1]/div[1]/a")

    @editStartDate = @browser.a(:xpath => "//input[@name='startDate']")
    @editReference = @browser.a(:xpath => "//input[@name='endDate']")


    @data = YAML.load_file('Data.yaml')
  end

  def clickGetPing
    @panelAchGetPing.wait_until_present
    @panelAchGetPing.click
  end

  def clickGetStatus
    @panelAchGetStatus.click
  end

  def clickGetCharges
    @panelGetCharges.click
  end

  def clickPostCharges
    @panelPostCharges.click
  end

  def clickGetChargesDetail
    @panelGetChargesDetails.click
  end

  def deleteCharges
    @panelDeleteCharges.click
  end

  def getCredits
    @panelGetCredits.click
  end

  def postCredits
    @panelPostCredits.click
  end

  def getCreditsDetails
    @panelGetCreditsDetails.click
  end

  def deleteCredits
    @panelDeleteCredits.click
  end

  def postCreditReference
    @panelPostCreditsReference.click
  end

  def getTransactions
    @panelGetTransactions.click
  end

  def getTransactionsDetails
    @panelGetTransactionsDetail.click
  end

  def getBatches
    @panelGetBatches.click
  end

  def getBatchesCurrent
    @panelGetBatchesCurrent.click
  end

  def getBatchesCurrentSummery
    @panelGetBatchesCurrentSummary.click
  end

  def getBatchesTotals
    @panelGetBatchesTotals.click
  end

  def postToken
    @panelPostToken.click
  end

  def putToken
    @panelPutToken.click
  end

  def deleteToken
    @panelDeleteToken.click
  end

  def editReference
    @panelResourceUrl.click
    #@browser.span(:xpath => ".//*[@id='method_url_panel']/div/p/span[2]/span/span[1]").set $TR_REFERENCE
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").click
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys [:control, "a"]
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys $TR_REFERENCE
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete

  end


  def editUrlWithToken
    @panelResourceUrl.click
    #@browser.span(:xpath => ".//*[@id='method_url_panel']/div/p/span[2]/span/span[1]").set $TR_REFERENCE
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").click
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys [:control, "a"]
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys $TR_TOKEN
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete

  end


  def setQueryParam(queryParameter,parameterValue)
     if(@browser.div(:id => "queryParametersTable")).visible?
       puts "  "
     else
       @panelQueryParameter.click
     end


    if(queryParameter == "type")
      @browser.select_list(:name => queryParameter).select parameterValue
    else
      @browser.text_field(:name => queryParameter).set parameterValue
    end
  end

  def setBody(jsonBody)
    @panelPostChargesBody.click
    @panelPostChargesBody.clear
    @panelPostChargesBody.set(jsonBody)

  end

  def setJsonBody(jsonBody)
    @panelPostChargesBody.wait_until_present
    @panelPostChargesBody.click
    #@panelPostChargesBody.send_keys [:control, 'a'], :backspace
    @browser.send_keys [:control, 'a'], :backspace
    sleep(5)
    @browser.send_keys jsonBody
    sleep(3)
    sleep(10)

  end


  def clickSendThisRequest
    sleep(3)
    @panelHeaderParam = @browser.i(:id => "headerParametersCollapse-arrow").wait_until_present
      sleep(3)
    begin
      @panelHeaderParam = @browser.h4(:xpath => "//h4[text()='Header Parameters']").click
      sleep 3
      @generateHMAC = @browser.button(:id => "regenerate-hmac-modal-btn").wait_until_present
      @generateHMAC = @browser.button(:id => "regenerate-hmac-modal-btn").click
      sleep 3
      @generateHMACModal = @browser.button(:id => "regenerate-hmac-submit-btn").wait_until_present
      @generateHMACModal = @browser.button(:id => "regenerate-hmac-submit-btn").click

    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      puts "HMAC Not generated."
    end

    @btnSendThisRequest.wait_until_present
    sleep(4)
    @btnSendThisRequest.click
  end

  def clickApplication
    @panelApplication.click
  end

  def verifyHomePageHeader()
    @tabDashBoard.wait_until_present
  end

  def verifyResponseCode(strResponseCode)
    sleep(5)
    @panelResponseCode.wait_until_present
    puts "Expected Response Code is - "+strResponseCode.to_s
    puts "Actual Response Code is - "+@panelResponseCode.text
    expect(@panelResponseCode.text).to  include (strResponseCode)
  end

  def verifyResponseBody(strResponseBody)
    puts "Expected response is - "+strResponseBody
    strActResponse = @panelResponseMessage.text
    expect(strActResponse).to include(strResponseBody)
  end

  def doAchSale(jsonBody)
    begin
      @APIDocPage = APIDocPage.new(@browser)
      @DPHomePage = DPHomePage.new(@browser)
      @APIDocPage.clickACH

      @panelPostCharges.click

      @panelPostChargesBody.wait_until_present
      @panelPostChargesBody.click
      @browser.send_keys [:control, 'a'], :backspace
      sleep(5)
      @browser.send_keys jsonBody
      sleep(3)
      @btnSendThisRequest.click
      sleep(10)
      @panelResponseMessage.wait_until_present
      @panelResponseMessage.text

      reference_index = (@panelResponseMessage.text).index('reference')
      $TR_REFERENCE = @panelResponseMessage.text[reference_index + 13, 10]
      puts "The reference No. is "+$TR_REFERENCE

      orderno_index = (@panelResponseMessage.text).index('orderNumber')
      $TR_ORDERNO = @panelResponseMessage.text[orderno_index + 15, 10]
      puts "The Order No. is "+$TR_ORDERNO

    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      puts "Reference No. not captured properly."
    end
    @DPHomePage.clickApiSandBox

  end

  def doAchCredit(jsonBody)
    begin
      @APIDocPage = APIDocPage.new(@browser)
      @DPHomePage = DPHomePage.new(@browser)
      @APIDocPage.clickACH

      @panelPostCredits.click

      @panelPostChargesBody.wait_until_present
      @panelPostChargesBody.click
      @browser.send_keys [:control, 'a'], :backspace
      sleep(5)
      @browser.send_keys jsonBody
      sleep(3)
      @btnSendThisRequest.click
      sleep(10)
      @panelResponseMessage.wait_until_present
      @panelResponseMessage.text

      reference_index = (@panelResponseMessage.text).index('reference')
      $TR_REFERENCE = @panelResponseMessage.text[reference_index + 13, 10]
      puts "The reference No. is "+$TR_REFERENCE

      orderno_index = (@panelResponseMessage.text).index('orderNumber')
      $TR_ORDERNO = @panelResponseMessage.text[orderno_index + 15, 10]
      puts "The Order No. is "+$TR_ORDERNO

    rescue
      puts "Reference No. not captured properly."
    end
    @DPHomePage.clickApiSandBox

  end

  def doPostToken(jsonBody)
    begin
      @APIDocPage = APIDocPage.new(@browser)
      @DPHomePage = DPHomePage.new(@browser)
      @APIDocPage.clickACH

      @panelPostToken.click

      @panelPostChargesBody.wait_until_present
      @panelPostChargesBody.click
      @browser.send_keys [:control, 'a'], :backspace
      sleep(5)
      @browser.send_keys jsonBody
      sleep(3)
      @btnSendThisRequest.click
      sleep(10)
      @panelResponseMessage.wait_until_present
      @panelResponseMessage.text

      reference_index = (@panelResponseMessage.text).index('data')
      $TR_TOKEN = @panelResponseMessage.text[reference_index + 8, 32]
      puts "The Token no. is "+$TR_TOKEN

    rescue
      puts "Reference No. not captured properly."
    end
    @DPHomePage.clickApiSandBox

  end

end