class SupportPage

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'watir'

  def initialize(browser)
    @browser = browser


    @DPHomeTab    = @browser.a(:xpath => "//a[text()='ACH']")
    @panelAchGetPing = @browser.a(:xpath => "//a[text()='get_ping']")

    @tabHome = @browser.a(:xpath => "//a[contains(text(),'Home']")
    @tabApiSandBox = @browser.a(:xpath =>"//a[contains(text(),'API Sandbox']")
    @tabDocumentation = @browser.a(:xpath =>"//a[text()='Documentation']")
    @tabSupport = @browser.a(:xpath =>"//a[text()='Support']")

    @txtEmail = @browser.text_field(:id => "edit-submitted-email")
    @txtDescription = @browser.textarea(:id => "edit-submitted-description-of-the-problem")
    #@btnSubmit = @browser.input(:xpath=> "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/form/div/div[4]/input")
    @btnSubmit = @browser.button(:name => "op")

    @toolTipFillThisField = @browser.a(:xpath =>"//a[text()='Please fill this field.']")
    @linkGoBackToForm = @browser.a(:xpath => "//a[text()='Go back to the form']")

    @data = YAML.load_file('Data.yaml')
  end

  def verifySupportPage
    verifyTextInPage("Contact Support")
    expect @txtEmail.present?
    expect @txtDescription.present?
    expect @btnSubmit.present?
  end


  def verifyTextInPage(text)
    #expect((@browser.text).to include(text)).to be_truthy,"The message not exists on page."
    expect(@browser.text).to include(text)
  end

  def clickSubmitButton
    sleep(5)
    @btnSubmit.click
    sleep(8)
  end

  def verifyToolTip
    expect @toolTipFillThisField.present?
  end

  def setEmail(strEmail)
    @txtEmail.set(strEmail)
  end

  def setDescrptions(strDescription)
    @txtDescription.set(strDescription)
  end


  def clickGoBackToFormLink
    @linkGoBackToForm.click
  end



end