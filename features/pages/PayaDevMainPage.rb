class PayaDevMainPage

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'watir'

  def initialize(browser)
    @browser = browser


    @DPHomeTab    = @browser.a(:xpath => "//a[text()='ACH']")
    @panelAchGetPing = @browser.a(:xpath => "//a[text()='get_ping']")

    @tabHome = @browser.a(:xpath => "//a[contains(text(),'Home']")
    @tabApiSandBox = @browser.a(:xpath =>"//a[contains(text(),'API Sandbox']")
    @tabDocumentation = @browser.a(:xpath =>"//a[text()='Documentation']")
    @tabSupport = @browser.a(:xpath =>"//a[text()='Support']")
    @tabMyApps = @browser.a(:xpath =>"//a[contains(text(),'My Apps']")
    @tabMyProfile = @browser.a(:xpath =>"//a[contains(text(),'My Profile']")
    @tabForums = @browser.a(:xpath =>"//a[text()='Forums']")
    @tabSignIn = @browser.a(:xpath =>"//a[text()='Sign In']")
    @tabCreateAccount = @browser.a(:xpath =>"//a[text()='Create Account']")

    @linkPaymentsJss = @browser.strong(:xpath =>"//*[@id='block-block-1']/div[2]/div/table/tbody/tr[2]/td[1]/h4/a/strong")
    @linkSageExchgVirDesk = @browser.strong(:xpath =>"//*[@id='block-block-1']/div[2]/div/table/tbody/tr[2]/td[2]/h4/a/strong")
    @linkSageExchgDesk = @browser.strong(:xpath =>"//*[@id='block-block-1']/div[2]/div/table/tbody/tr[2]/td[3]/h4/a/strong")
    @linkDirectAPIs = @browser.strong(:xpath =>"//*[@id='block-block-1']/div[2]/div/table/tbody/tr[2]/td[4]/h4/a/strong")
    @linkAppAPIs = @browser.strong(:xpath =>"//*[@id='block-block-1']/div[2]/div/table/tbody/tr[2]/td[5]/h4/a/strong")
    @linkMobileSDKs = @browser.strong(:xpath =>"//*[@id='block-block-6']/div[2]/div/div[1]/div[2]/h3/strong")
    @linkAdvancedFraudAPIs = @browser.strong(:xpath =>"//*[@id='block-block-6']/div[2]/div/div[2]/div[2]/h3/strong")
    @linkSageConnect = @browser.strong(:xpath =>"//*[@id='block-block-6']/div[2]/div/div[3]/div[2]/h3/strong")
    @titleAllPages = @browser.strong(:xpath =>"//*[@class='page_title']")

    @linkModalUI = @browser.a(:xpath => "//a[text()='01 - Modal UI']")
    @linkInlineUI = @browser.a(:xpath => "//a[text()='02 - Inline UI']")
    @linkCustomUI = @browser.a(:xpath => "//a[text()='03 - Custom UI']")
    @linkTokanization = @browser.a(:xpath => "//a[text()='04 - Tokenization']")
    @linkPayaDirectAPI = @browser.a(:xpath => "//a[text()='Paya Direct API']")
    @linkSED2 = @browser.a(:xpath => "//a[text()='Sage Exchange Desktop v2.0']")
    @linkPayaAppAPI = @browser.a(:xpath => "//a[text()='Paya Application API']")
    @linkSEVD2 = @browser.a(:xpath => "//a[text()='Sage Exchange Virtual Desktop v2.0']")
    @linkSagePaymentEFT = @browser.a(:xpath => "//a[text()='Sage Payments EFT']")
    @linkGiftAndLoyalty = @browser.a(:xpath => "//a[text()='Gift & Loyalty ']")
    @linkSageEFTCheckServices = @browser.a(:xpath => "//a[text()='Sage EFT Check Services']")
    @linkCheckAndACH = @browser.a(:xpath => "//a[text()='Check & ACH']")


    @data = YAML.load_file('Data.yaml')
  end

  def clickDocumentation
    @tabDocumentation.click

  end

  def clickSupport
    @tabSupport.click
  end

  def clickForums
    @tabForums.click
  end

  def clickSignIn
    @tabSignIn.click
  end

  def clickCreateAccount
    @tabCreateAccount.click
  end

  def clickPaymentJS
    @linkPaymentsJss.click
  end

  def clickSageExchangeVirtualDesk
    @linkSageExchgVirDesk.click
  end

  def clickSageExchangeDeskTop
    @linkSageExchgDesk.click
  end


  def clickDirectAPIs
    @linkDirectAPIs.click
  end

  def clickApplicationAPIs
    @linkAppAPIs.click
  end

  def verifyTextInPage(text)
    expect(@browser.text).to include(text)
    @browser.back
  end

  def verifyDocumentationPage
    @linkModalUI.click
    sleep(5)
    verifyTextInPage("Modal UI")
    @linkInlineUI.click
    sleep(5)
    verifyTextInPage("When PaymentsJS is initialized to a <div>, the UI appears within that element")
    @linkCustomUI.click
    sleep(5)
    verifyTextInPage("If you have specific UI requirements, you can build your own payment form and power it with PaymentsJS")
    @linkTokanization.click
    sleep(5)
    verifyTextInPage("Not ready to charge a card, or expecting to charge it multiple times")
    @linkSED2.click
    sleep(5)
    verifyTextInPage("Sage Exchange Desktop is an Installed .Net application which is fully PCI-DSS compliant")
    @linkPayaAppAPI.click
    sleep(5)
    verifyTextInPage("Merchant Application API")
    @linkSEVD2.click
    sleep(5)
    verifyTextInPage("Similar to our Desktop product, the SEVD")
    @linkSagePaymentEFT.click
    sleep(5)
    verifyTextInPage("Sage EFT Developers")
    @linkGiftAndLoyalty.click
    sleep(5)
    verifyTextInPage("Easily add Gift and Loyalty card support to your platform by utilizing this API. Full integration of card issuance")
    @linkSageEFTCheckServices.click
    sleep(5)
    verifyTextInPage("We are still working on migrating our EFT platform APIs")
    @linkCheckAndACH.click
    sleep(5)
    verifyTextInPage("Build check processing and ACH Debit/Credit functionality directly into your application")

  end


end