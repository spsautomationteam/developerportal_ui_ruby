class ApplicationsPage

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'watir'

  def initialize(browser)
    @browser = browser
    @DPHomeTab    = @browser.a(:xpath => "//a[text()='ACH']")
    @panelAchGetPing = @browser.a(:xpath => "//a[text()='get_ping']")
    @panelAch = @browser.a(:xpath => "//a[text()='ACH']")
    @panelAchGetStatus = @browser.a(:xpath => "//a[text()='get_status']")
    @btnSendThisRequest = @browser.button(:xpath => "//button[text()='Send this request']")
    @panelResponseCode = @browser.strong(:xpath => ".//*[@id='request_response_container']/div[1]/strong")
    #@panelResponseMessage = @browser.pre(:xpath => ".//*[@id='request_response_container']/div[1]/pre")
    #@panelResponseMessage = @browser.code(:id => "some-code123")
    @panelResponseMessage = @browser.pre(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[6]/div[1]/pre")



    #Application
    @panelTemplatesGetAddons = @browser.a(:xpath => "//a[text()='Templates_GetAddons']")
    @panelTemplatesGetAdvanceFundingProcessors = @browser.a(:xpath => "//a[text()='Templates_GetAdvanceFundingProcessors']")
    @panelTemplatesGetAssociations = @browser.a(:xpath => "//a[text()='Templates_GetAssociations']")
    @panelTemplatesGetDiscountPaidFrequencies = @browser.a(:xpath => "//a[text()='Templates_GetDiscountPaidFrequencies']")
    @panelTemplatesGetEquipment = @browser.a(:xpath => "//a[text()='Templates_GetEquipment']")
    @panelTemplatesGetEquipmentPrograms = @browser.a(:xpath => "//a[text()='Templates_GetEquipmentPrograms']")
    @panelTemplatesGetFanfTypes = @browser.a(:xpath => "//a[text()='Templates_GetFanfTypes']")
    @panelTemplatesGetFees = @browser.a(:xpath => "//a[text()='Templates_GetFees']")
    @panelTemplatesGetLeadSources = @browser.a(:xpath => "//a[text()='Templates_GetLeadSources']")
    @panelTemplatesGetPinDebitInterchangeTypes = @browser.a(:xpath => "//a[text()='Templates_GetPinDebitInterchangeTypes']")
    @panelTemplatesGetProducts = @browser.a(:xpath => "//a[text()='Templates_GetProducts']")
    @panelTemplatesGetReferralGroups = @browser.a(:xpath => "//a[text()='Templates_GetReferralGroups']")
    @panelTemplatesGetSettlementTypes = @browser.a(:xpath => "//a[text()='Templates_GetSettlementTypes']")

    #@panelPostChargesBody = @browser.a(:xpath => ".//*[@id='request_payload']/div/div/div[2]/div/div[5]/div/div[1]/div/div/div/div[3]/div[1]")
    @panelPostChargesBody = @browser.pre(:xpath => ".//*[@id='request_payload']/div/div/div[2]/div/div[5]/div/div[1]/div/div/div/div[3]/div[1]/pre")

    #@panelPostChargesBody = @browser.div(:xpath => ".//*[@id='request_payload']/div/div/div[2]/div/div[5]/div/div[1]/div/div")


    @spanReference = @browser.a(:xpath => ".//*[@id='method_url_panel']/div/p/span[2]/span/span[1]")
    @panelQueryParameter = @browser.a(:xpath => ".//*[@id='accordion']/div[2]/div[1]/a")
    @panelResourceUrl = @browser.a(:xpath => ".//*[@id='accordion']/div[1]/div[1]/a")


    @data = YAML.load_file('Data.yaml')
  end

  def clickTemplatesGetAddons
    @panelTemplatesGetAddons.click
  end

  def clickTemplatesGetAdvanceFundingProcessors
    @panelTemplatesGetAdvanceFundingProcessors.click
  end

  def clickTemplatesGetAssociations
    @panelTemplatesGetAssociations.click
  end

  def clickTemplatesGetDiscountPaidFrequencies
    @panelTemplatesGetDiscountPaidFrequencies.click
  end

  def clickTemplatesGetEquipment
    @panelTemplatesGetEquipment.click
  end

  def clickTemplatesGetEquipmentPrograms
    @panelTemplatesGetEquipmentPrograms.click
  end

  def clickTemplatesGetFanfTypes
    @panelTemplatesGetFanfTypes.click
  end

  def clickTemplatesGetFees
    @panelTemplatesGetFees.click
  end

  def clickTemplatesGetLeadSources
    @panelTemplatesGetLeadSources.click
  end

  def clickTemplatesGetPinDebitInterchangeTypes
    @panelTemplatesGetPinDebitInterchangeTypes.click
  end

  def clickTemplatesGetProducts
    @panelTemplatesGetProducts.click
  end

  def clickTemplatesGetReferralGroups
    @panelTemplatesGetReferralGroups.click
  end

  def clickTemplatesGetSettlementTypes
    @panelTemplatesGetSettlementTypes.click
  end

  def clickSendRequestBtn
    @btnSendThisRequest.click
  end

end