require 'rubygems'
require 'rspec'
#require 'watir-webdriver'
require 'watir'

class APIDocPage
  attr_accessor :loginTab,:txtUsername,:txtPassword,:btnLogin

  def initialize(browser)
    @browser = browser
    @DPHomeTab    = @browser.a(:Xpath => "//a[text()='ACH']")
    @panelAch = @browser.a(:xpath => "//a[text()='ACH']")
    @panelBankCard = @browser.a(:xpath => "//a[text()='Bankcard']")
    @panelToken = @browser.a(:xpath => "//a[text()='Token']")
    @panelApplication = @browser.a(:xpath => "//a[text()='Application']")
    @data = YAML.load_file('Data.yaml')
  end


  def clickACH
    @panelAch.click
  end

  def clickBankCard
    @panelBankCard.click
  end

  def clickToken
    @panelToken.click
  end

  def clickApplication
    @panelApplication.click
  end

  def verifyHomePageHeader()
    @tabDashBoard.wait_until_present
  end

end