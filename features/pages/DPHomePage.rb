require 'rubygems'
require 'rspec'
#require 'watir-webdriver'
require 'watir'
require 'win32ole'

class DPHomePage
  attr_accessor :loginTab,:txtUsername,:txtPassword,:btnLogin

  def initialize(browser)
    @browser = browser
    @DPHomeTab    = @browser.a(:xpath => "//a[text()='API Sandbox']")
    @ApiSandBoxLink = @browser.a(:xpath => "//a[text()='API Sandbox']")
    @data = YAML.load_file('Data.yaml')
  end

  def visit()
    @browser.goto "https://developer.sagepayments.com"
#    au3 = WIN32OLE.new("AutoItX3.Control")
#    au3.Send("pradeep.ande@sage.com")
#    au3.Send("{TAB}")
#    au3.Send("Sage@123")
#    @browser.alert.ok
    sleep 5
  end

  def clickApiSandBox
    @ApiSandBoxLink.click
  end

  def verifyHomePageHeader()
    @tabDashBoard.wait_until_present
  end
end