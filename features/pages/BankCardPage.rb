class BankCardPage

  require 'rspec/expectations'
  require 'rspec/core'
  require 'rspec/collection_matchers'
  include RSpec::Matchers
  require 'watir'

  def initialize(browser)
    @browser = browser
    @DPHomeTab    = @browser.a(:xpath => "//a[text()='ACH']")
    @panelAchGetPing = @browser.a(:xpath => "//a[text()='get_ping']")
    @panelAch = @browser.a(:xpath => "//a[text()='ACH']")
    @panelAchGetStatus = @browser.a(:xpath => "//a[text()='get_status']")
    @btnSendThisRequest = @browser.button(:xpath => "//button[text()='Send this request']")
    @panelResponseCode = @browser.strong(:xpath => ".//*[@id='request_response_container']/div[1]/strong")
    #@panelResponseMessage = @browser.pre(:xpath => ".//*[@id='request_response_container']/div[1]/pre")
    #@panelResponseMessage = @browser.code(:id => "some-code123")
    @panelResponseMessage = @browser.pre(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[6]/div[1]/pre")



    @panelGetCharges = @browser.a(:xpath => "//a[text()='get_charges']")
    @panelPostCharges = @browser.a(:xpath => "//a[text()='post_charges']")
    @panelGetChargesDetails =  @browser.a(:xpath => "//a[text()='get_charges_detail']")
    @panelPutCharges = @browser.a(:xpath => "//a[text()='put_charges']")
    @panelDeleteCharges = @browser.a(:xpath => "//a[text()='delete_charges']")
    @panelPostChargesLineitems = @browser.a(:xpath => "//a[text()='post_charge_lineitems']")
    @panelGetChargesLineitems = @browser.a(:xpath => "//a[text()='get_charges_lineitems_detail']")
    @panelDeleteChargeLineitems = @browser.a(:xpath => "//a[text()='delete_charge_lineitems']")
    @panelPostCredits = @browser.a(:xpath => "//a[text()='post_credits']")
    @panelGetCredits = @browser.a(:xpath => "//a[text()='get_credits']")
    @panelGetCreditsDetails = @browser.a(:xpath => "//a[text()='get_credits_detail']")
    @panelPostCreditsReference = @browser.a(:xpath => "//a[text()='post_credits_reference']")
    @panelDeleteCredits = @browser.a(:xpath => "//a[text()='delete_credits']")
    @panelGetTransactions = @browser.a(:xpath => "//a[text()='get_transactions']")
    @panelGetTransactionsDetail = @browser.a(:xpath => "//a[text()='get_transactions_detail']")
    @panelGetBatches = @browser.a(:xpath => "//a[text()='get_batches']")
    @panelGetBatchesCurrent = @browser.a(:xpath => "//a[text()='get_batches_current']")
    @panelPostBatchesCurrent = @browser.a(:xpath => "//a[text()='post_batches_current']")
    @panelGetBatchesCurrentSummary = @browser.a(:xpath => "//a[text()='get_batches_current_summary']")
    @panelGetBatchesTotals = @browser.a(:xpath => "//a[text()='get_batches_totals']")
    @panelGetBatchesReference = @browser.a(:xpath => "//a[text()='get_batches_reference']")

    @panelPostToken = @browser.a(:xpath => "//a[text()='post-tokens']")
    @panelPutToken = @browser.a(:xpath => "//a[text()='put-token']")
    @panelDeleteToken = @browser.a(:xpath => "//a[text()='delete-token']")

    #@panelPostChargesBody = @browser.a(:xpath => ".//*[@id='request_payload']/div/div/div[2]/div/div[5]/div/div[1]/div/div/div/div[3]/div[1]")
    @panelPostChargesBody = @browser.pre(:xpath => ".//*[@id='request_payload']/div/div/div[2]/div/div[5]/div/div[1]/div/div/div/div[3]/div[1]/pre")

    #@panelPostChargesBody = @browser.div(:xpath => ".//*[@id='request_payload']/div/div/div[2]/div/div[5]/div/div[1]/div/div")


    @spanReference = @browser.a(:xpath => ".//*[@id='method_url_panel']/div/p/span[2]/span/span[1]")
    @panelQueryParameter = @browser.a(:xpath => ".//*[@id='accordion']/div[2]/div[1]/a")
    @panelResourceUrl = @browser.a(:xpath => ".//*[@id='accordion']/div[1]/div[1]/a")

    @editStartDate = @browser.a(:xpath => "//input[@name='startDate']")
    @editReference = @browser.a(:xpath => "//input[@name='endDate']")


    @data = YAML.load_file('Data.yaml')
  end

  def clickPutCharges
    @panelPutCharges.click
  end

  def postChargeLineitems
    @panelPostChargesLineitems.click
  end

  def getChargesLineItemsDetail
    @panelGetChargesLineitems.click
  end

  def deleteChargeLineitems
    @panelDeleteChargeLineitems.click
  end

  def postBatchesCurrent
    @panelPostBatchesCurrent.click
  end

  def getBatchesReference
    @panelGetBatchesReference.click
  end

  def clickAPISandBox
    @DPHomePage.clickApiSandBox
  end

  def clickToken
    @DPHomePage.clickToken
  end


  def doBankCardSale(jsonBody, trType)
  #begin
    @APIDocPage = APIDocPage.new(@browser)
    @DPHomePage = DPHomePage.new(@browser)
    @APIDocPage.clickBankCard
    @ACHPage = ACHPage.new(@browser)

    @panelPostCharges.click
    sleep(2)

    @ACHPage.setQueryParam("type", trType)


    @panelPostChargesBody.wait_until_present
    @panelPostChargesBody.click
    #@panelPostChargesBody.send_keys [:control, 'a'], :backspace
    @browser.send_keys [:control, 'a'], :backspace
    sleep(5)
    #@panelPostChargesBody.send_keys jsonBody
    @browser.send_keys jsonBody
    sleep(3)
    @ACHPage.clickSendThisRequest
    sleep(10)
    @panelResponseMessage.wait_until_present
    @panelResponseMessage.text

    reference_index = (@panelResponseMessage.text).index('reference')
    $TR_REFERENCE = @panelResponseMessage.text[reference_index + 13, 10]
    puts "The reference No. is "+$TR_REFERENCE

    orderno_index = (@panelResponseMessage.text).index('orderNumber')
    $TR_ORDERNO = @panelResponseMessage.text[orderno_index + 15, 10]
    puts "The Order No. is "+$TR_ORDERNO

  #rescue
    puts "Reference No. not captured properly."
  #end
  @DPHomePage.clickApiSandBox

  end


  def doBankCardCredit(jsonBody)
    #begin
    @APIDocPage = APIDocPage.new(@browser)
    @DPHomePage = DPHomePage.new(@browser)
    @APIDocPage.clickBankCard
    @ACHPage = ACHPage.new(@browser)

    @panelPostCredits.click
    sleep(2)
    @panelPostChargesBody.wait_until_present
    @panelPostChargesBody.click
    #@panelPostChargesBody.send_keys [:control, 'a'], :backspace
    @browser.send_keys [:control, 'a'], :backspace
    sleep(5)
    #@panelPostChargesBody.send_keys jsonBody
    @browser.send_keys jsonBody
    sleep(5)
    #@panelPostChargesBody.send_keys jsonBody
    @ACHPage.clickSendThisRequest
    sleep(10)
    @panelResponseMessage.wait_until_present
    @panelResponseMessage.text

    reference_index = (@panelResponseMessage.text).index('reference')
    $TR_REFERENCE = @panelResponseMessage.text[reference_index + 13, 10]
    puts "The reference No. is "+$TR_REFERENCE

    orderno_index = (@panelResponseMessage.text).index('orderNumber')
    $TR_ORDERNO = @panelResponseMessage.text[orderno_index + 15, 10]
    puts "The Order No. is "+$TR_ORDERNO

    #rescue
    puts "Reference No. not captured properly."
    #end
    @DPHomePage.clickApiSandBox

  end

  def doPostBatches(jsonBody)
    #begin
    @APIDocPage = APIDocPage.new(@browser)
    @DPHomePage = DPHomePage.new(@browser)
    @APIDocPage.clickBankCard
    @ACHPage = ACHPage.new(@browser)

    @panelPostBatchesCurrent.click
    sleep(2)
    @panelPostChargesBody.wait_until_present
    @panelPostChargesBody.click
    @browser.send_keys [:control, 'a'], :backspace
    sleep(5)
    @browser.send_keys jsonBody
    sleep(3)
    @ACHPage.clickSendThisRequest
    sleep(10)
    @panelResponseMessage.wait_until_present
    @panelResponseMessage.text

    reference_index = (@panelResponseMessage.text).index('reference')
    $BCH_REFERENCE = @panelResponseMessage.text[reference_index + 13, 10]
    puts "The Batch reference No. is "+$BCH_REFERENCE

    #rescue
    puts "Reference No. not captured properly."
    #end
    @DPHomePage.clickApiSandBox

  end

  def doPostToken(jsonBody)
    #begin
    @APIDocPage = APIDocPage.new(@browser)
    @DPHomePage = DPHomePage.new(@browser)
    @APIDocPage.clickToken
    @ACHPage = ACHPage.new(@browser)

    @panelPostToken.click
    sleep(2)
    @panelPostChargesBody.wait_until_present
    @panelPostChargesBody.click
    @browser.send_keys [:control, 'a'], :backspace
    sleep(5)
    @browser.send_keys jsonBody
    sleep(3)
    @ACHPage.clickSendThisRequest
    sleep(10)
    @panelResponseMessage.wait_until_present
    @panelResponseMessage.text

    reference_index = (@panelResponseMessage.text).index('data')
    $TR_TOKEN = @panelResponseMessage.text[reference_index + 8, 32]
    puts "The BankCard token. is "+$TR_TOKEN
    @DPHomePage.clickApiSandBox

  end

  def setJsonBody(jsonBody)
    @panelPostChargesBody.wait_until_present
    @panelPostChargesBody.click
    @browser.send_keys [:control, 'a'], :backspace
    sleep(5)
    @browser.send_keys jsonBody
    sleep(10)
  end

  def editBatchReference
    @panelResourceUrl.click
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").click
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys [:control, "a"]
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys $TR_REFERENCE
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete
    sleep(1)
    @browser.span(:xpath => "html/body/div[2]/div[3]/div/div/div/div[2]/div[2]/div/div/div/div/div[1]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/p/span[2]/span/span[1]").send_keys :delete

  end


end
