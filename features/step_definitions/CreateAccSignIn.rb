And(/^Click on 'Create Account' tab$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickCreateAccount
end

And(/^Enter valid data in all required fields$/) do
  @CreateAccSignInPage = CreateAccSignInPage.new(@browser)
  @CreateAccSignInPage.editFirstName(YAML.load_file('Data.yaml')['FirstName'])
  @CreateAccSignInPage.editLastName(YAML.load_file('Data.yaml')['LastName'])
  @CreateAccSignInPage.editUserName(YAML.load_file('Data.yaml')['Username'])
  @CreateAccSignInPage.editEmail(YAML.load_file('Data.yaml')['EmailAddress'])
  @CreateAccSignInPage.editCompanyName(YAML.load_file('Data.yaml')['CompanyName'])
  @CreateAccSignInPage.selectCountry(YAML.load_file('Data.yaml')['Country'])
  @CreateAccSignInPage.editAddress1(YAML.load_file('Data.yaml')['Address1'])
  @CreateAccSignInPage.editAddress2(YAML.load_file('Data.yaml')['Address2'])
  @CreateAccSignInPage.editCity(YAML.load_file('Data.yaml')['City'])
  @CreateAccSignInPage.selectState(YAML.load_file('Data.yaml')['State'])
  @CreateAccSignInPage.editZipCode(YAML.load_file('Data.yaml')['ZipCode'])
  @CreateAccSignInPage.chkTermsCondition

end

And(/^Click on Create Account button$/) do
  @CreateAccSignInPage = CreateAccSignInPage.new(@browser)
  @CreateAccSignInPage.clickCreateAccountBtn
  sleep 5
end

And(/^It should create new account and should display welcome message$/) do
  @CreateAccSignInPage = CreateAccSignInPage.new(@browser)
  @CreateAccSignInPage.verifyTextInPage("Thank you")
  @CreateAccSignInPage.verifyTextInPage("A welcome message with further instructions has been sent to your e-mail address.")
  @CreateAccSignInPage.verifyTextInPage("We really appreciate you registering with Sage payments Solutions.")
  sleep 5
end

And(/^Click on Signin button$/) do
  @CreateAccSignInPage = CreateAccSignInPage.new(@browser)
  @CreateAccSignInPage.clickSignIn
  sleep 5
end


And(/^I enter Developer Portal username "([^"]*)"$/) do |strEmail|
  @CreateAccSignInPage = CreateAccSignInPage.new(@browser)
  @CreateAccSignInPage.setUserName(strEmail)

end

And(/^I enter Developer Portal password "([^"]*)"$/) do |strPassword|
  @CreateAccSignInPage = CreateAccSignInPage.new(@browser)
  @CreateAccSignInPage.setPassword(strPassword)

end