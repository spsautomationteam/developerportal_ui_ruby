
Given(/^I launch (.*)$/) do |url|
  @LoginPage = LoginPage.new(@browser)
  @LoginPage.visit

end

And(/^I click on Login tab$/) do
  @LoginPage.clickLoginTab
end

And(/^I enter username (.*)$/) do |user_name|
  @LoginPage.enterUsername(user_name)
end

And(/^I enter password (.*)$/) do |password|
  @LoginPage.enterPassword(password)
end

When(/^I click Login button$/) do
  @LoginPage.clickLoginButton
end

Then(/^I see Home page$/) do
  @LoginPage.verifyHomePageHeader
end

Given(/^Developer Portal is opened$/) do
  @DPHomePage = DPHomePage.new(@browser)
  @DPHomePage.visit
  sleep(10)
  @DPHomePage.clickApiSandBox
end

And(/^I select ach$/) do
  @APIDocPage = APIDocPage.new(@browser)
  @APIDocPage.clickACH
end

And(/^I select get_ping$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.clickGetPing
end

And(/^I select get_status$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.clickGetStatus
end

And(/^I select get_charges$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.clickGetCharges
end

And(/^I select post_charges$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.clickPostCharges
end

And(/^I select get_charges_detail$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.clickGetChargesDetail
end

And(/^I select delete_charges$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.deleteCharges
end

And(/^I select get_credits$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.getCredits
end

And(/^I select post_credits$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.postCredits
end

And(/^I select get_credits_detail$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.getCreditsDetails
end

And(/^I select delete_credits$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.deleteCredits
end

And(/^I select post_credits_reference$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.postCreditReference
end

And(/^I select get_transactions$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.getTransactions
end

And(/^I select get_transactions_detail$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.getTransactionsDetails
end

And(/^I select get_batches$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.getBatches
end

And(/^I select get_batches_current$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.getBatchesCurrent
end

And(/^I select get_batches_totals$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.getBatchesTotals
end

And(/^I select get_batches_current_summary$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.getBatchesCurrentSummery
end

And(/^I select post-tokens$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.postToken
end

And(/^I select put-token$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.putToken
end

And(/^I select delete-token$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.deleteToken
end

And(/^I edit resource URL with transaction reference$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.editReference
end

And(/^I edit resource URL with token$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.editUrlWithToken
end

And(/^I set body to (.*)$/) do |jsonBody|
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.setJsonBody(jsonBody)
end

And(/^I set query parameter "([^"]*)" to "([^"]*)"$/) do |queryParameter, parameterValue|
  @ACHPage = ACHPage.new(@browser)
  if parameterValue == "TR_REFERENCE"
    @ACHPage.setQueryParam(queryParameter, $TR_REFERENCE)
  elsif parameterValue == "TR_ORDERNO"
    @ACHPage.setQueryParam(queryParameter, $TR_ORDERNO)
  else
    @ACHPage.setQueryParam(queryParameter, parameterValue)
  end

end

And(/^I do ACH sale using body (.*)$/) do |jsonBody|
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.doAchSale(jsonBody)
end

And(/^I do ACH credit using body (.*)$/) do |jsonBody|
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.doAchCredit(jsonBody)
end

And(/^I do ACH post_token using body (.*)$/) do |jsonBody|
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.doPostToken(jsonBody)
end

When(/^I click on Send this request button$/) do
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.clickSendThisRequest
end

Then(/^response code should be (\d+)$/) do |strResponseCode|
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.verifyResponseCode(strResponseCode)
end

And(/^response body should contain "([^"]*)"$/) do |strResponseBody|
  @ACHPage = ACHPage.new(@browser)
  @ACHPage.verifyResponseBody(strResponseBody)
end

And(/^response body should contain "([^"]*)": "([^"]*)"$/) do |response, resp_value|

  if resp_value == "TR_REFERENCE"
    @ACHPage.verifyResponseBody(response+'": "'+$TR_REFERENCE)
  elsif resp_value == "TR_ORDERNO"
    @ACHPage.verifyResponseBody(response+'": "'+$TR_ORDERNO)
  elsif resp_value == "TR_TOKEN"
    @ACHPage.verifyResponseBody(response+'": "'+$TR_TOKEN)
  else

    @ACHPage.verifyResponseBody(response+'": "'+resp_value)
  end
end

And(/^response body should contain "([^"]*)": (\d+)$/) do |response, resp_value|
  @ACHPage.verifyResponseBody(response+'"'": "+resp_value)
end
