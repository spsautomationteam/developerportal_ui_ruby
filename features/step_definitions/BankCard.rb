And(/^I select BankCard$/) do
  @APIDocPage = APIDocPage.new(@browser)
  @APIDocPage.clickBankCard
end

And(/^I do sale charges using body (.*)$/) do |jsonBody|
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.doBankCardSale(jsonBody, "Sale")
end

And(/^I do Authorization using body (.*)$/) do |jsonBody|
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.doBankCardSale(jsonBody, "Authorization")
end

And(/^I do credit using body (.*)$/) do |jsonBody|
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.doBankCardCredit(jsonBody)
end

And(/^I do post batches using body (.*)$/) do |jsonBody|
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.doPostBatches(jsonBody)
end

And(/^I select put_charges$/) do
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.clickPutCharges
end

And(/^I set json body to (.*)$/) do |jsonBody|
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.setJsonBody(jsonBody)
end

And(/^I select post_charge_lineitems$/) do
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.postChargeLineitems
end

And(/^I select get_charges_lineitems_detail$/) do
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.getChargesLineItemsDetail
end

And(/^I select delete_charge_lineitems$/) do
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.deleteChargeLineitems
end

And(/^I select post_batches_current$/) do
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.postBatchesCurrent
end

And(/^I select get_batches_reference$/) do
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.getBatchesReference
end

And(/^I click on APISandBox$/) do
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.clickAPISandBox
end

And(/^I edit resource URL with batch reference$/) do
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.editBatchReference
end

And(/^I select Token$/) do
  @APIDocPage = APIDocPage.new(@browser)
  @APIDocPage.clickToken
end

And(/^I do BankCard post_token using body (.*)$/) do |jsonBody|
  @BankCardPage = BankCardPage.new(@browser)
  @BankCardPage.doPostToken(jsonBody)
end