Given(/^Launch Sage Developer portal URL$/) do
  pending # Write code here that turns the phrase above into concrete actions
end

When(/^Click on 'tabApiSandBox' Tab present at top of the Page$/) do
  @DPHomePage = DPHomePage.new(@browser)
  @DPHomePage.clickApiSandBox
end

Then(/^Verify application should navigate to 'API Documentation' page properly$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("API Documentation")
end

When(/^Click on 'tabDocumentation' Tab present at top of the Page$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickDocumentation
end

Then(/^Verify application should navigate to 'Documentation' page properly$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("Documentation")
end

When(/^Click on 'tabForums' Tab present at top of the Page$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickForums

end

Then(/^Verify application should navigate to 'Forums' page properly$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("Forums")
end

When(/^Click on 'tabSupport' Tab present at top of the Page$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickSupport

end

Then(/^Verify application should navigate to 'Contact Support' page properly$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("Contact Support")
end

When(/^Click on 'tabCreateAccount' Tab present at top of the Page$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickCreateAccount

end

Then(/^Verify application should navigate to 'Create Account' page properly$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("Create Account")
end

When(/^Click on 'tabSignIn' Tab present at top of the Page$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickSignIn

end

Then(/^Verify application should navigate to 'Sign In' page properly$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("Sign In")
end

When(/^Click on 'linkPaymentsJss' link present at middle of the Page$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickPaymentJS

end

Then(/^Verify application should navigate to 'Payments\.js' page properly$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("Payments\.js")
end

When(/^Click on 'linkSageExchgVirDesk' link present at middle of the Page$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickSageExchangeVirtualDesk

end

Then(/^Verify application should navigate to 'Sage Exchange Virtual Desktop v(\d+)\.(\d+)' page properly$/) do |arg1, arg2|
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("Sage Exchange Virtual Desktop v2.0")
end

When(/^Click on 'linkSageExchgDesk' link present at middle of the Page$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickSageExchangeDeskTop

end

Then(/^Verify application should navigate to 'Sage Exchange Desktop v(\d+)\.(\d+)' page properly$/) do |arg1, arg2|
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("Sage Exchange Desktop")
end

When(/^Click on 'linkDirectAPIs' link present at middle of the Page$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickDirectAPIs
end

Then(/^Verify application should navigate to 'Paya Direct API' page properly$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("Paya Direct API")
end

When(/^Click on 'linkAppAPIs' link present at middle of the Page$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.clickApplicationAPIs
end

Then(/^Verify application should navigate to 'Paya Application API' page properly$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage("Paya Application API")
end

Then(/^I click on all the links and verify all the pages are opened properly$/) do
  @PayaDevMainPage = PayaDevMainPage.new(@browser)
  @PayaDevMainPage.verifyDocumentationPage
end

