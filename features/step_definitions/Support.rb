And(/^Verify It should display the required options under 'Support' page$/) do
  @SupportPage = SupportPage.new(@browser)
  @SupportPage.verifySupportPage
end

And(/^Click on SUBMIT button$/) do
  @SupportPage = SupportPage.new(@browser)
  @SupportPage.clickSubmitButton
end

And(/^Verify tool tip 'Please fill out this field'$/) do
  @SupportPage = SupportPage.new(@browser)
  @SupportPage.verifyToolTip
end

And(/^Enter valid email "([^"]*)"$/) do |strEmail|
  @SupportPage = SupportPage.new(@browser)
  @SupportPage.setEmail(strEmail)

end

And(/^Enter description "([^"]*)"$/) do |strDescription|
  @SupportPage = SupportPage.new(@browser)
  @SupportPage.setDescrptions(strDescription)
end

And(/^Verify response message "([^"]*)"$/) do |strMessage|
  @PayaDevMainPage = SupportPage.new(@browser)
  @PayaDevMainPage.verifyTextInPage(strMessage)
end

And(/^Click on 'Go back to the form' link$/) do
  @SupportPage = SupportPage.new(@browser)
  @SupportPage.clickGoBackToFormLink
end

