And(/^I select Application$/) do
  @APIDocPage = APIDocPage.new(@browser)
  @APIDocPage.clickApplication
end

And(/^I select Templates_GetAddons$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetAddons
end

And(/^I select Templates_GetAdvanceFundingProcessors$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetAdvanceFundingProcessors
end

And(/^I select Templates_GetAssociations$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetAssociations
end

And(/^I select Templates_GetDiscountPaidFrequencies$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetDiscountPaidFrequencies
end

And(/^I select Templates_GetEquipment$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetEquipment
end

And(/^I select Templates_GetEquipmentPrograms$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetEquipmentPrograms
end

And(/^I select Templates_GetFanfTypes$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetFanfTypes
end

And(/^I select Templates_GetFees$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetFees
end

And(/^I select Templates_GetLeadSources$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetLeadSources
end

And(/^I select Templates_GetPinDebitInterchangeTypes$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetPinDebitInterchangeTypes
end

And(/^I select Templates_GetProducts$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetProducts
end

And(/^I select Templates_GetReferralGroups$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetReferralGroups
end

And(/^I select Templates_GetSettlementTypes$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickTemplatesGetSettlementTypes
end

And(/^I click on Send request button$/) do
  @ApplicationsPage = ApplicationsPage.new(@browser)
  @ApplicationsPage.clickSendRequestBtn
end
